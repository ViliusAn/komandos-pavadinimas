CREATE TABLE `Atostogos` (
  `id` int,
  `pradzios_data` date,
  `pabaigos_data` date,
  `tipas` ENUM ('kasmetinės', 'tikslinės', 'papildomos'),
  `darbuotojas` int
);

CREATE TABLE `Adresas` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `gatve` varchar(255),
  `pasto_kodas` int,
  `miestas` varchar(255),
  `salis` varchar(255)
);

CREATE TABLE `Darbuotojas` (
  `asmens_kodas` int PRIMARY KEY,
  `pareigos` ENUM ('sandelininkas', 'vadybininkas', 'isveziotojas', 'administratorius'),
  `telefono_nr` varchar(255),
  `bankos_saskaitos_nr` varchar(255)
);

CREATE TABLE `Dokumentas` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `pavadinimas` varchar(255),
  `tipas` ENUM ('darbo sutartis', 'atsotogų sutartis', 'konfidencialumo sutartis', 'apmokejimu sutartis', 'nusižengimas', 'kita'),
  `sudarymo_data` date,
  `url` varchar(255),
  `aprasymas` varchar(255),
  `pasirase` int,
  `patvirtino` int
);

CREATE TABLE `Marsrutas` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `sandelis_id` int,
  `inventorius_id` int,
  `kryptis` ENUM ('is_sandelio', 'i_sandeli'),
  `Atvykimas` datetime
);

CREATE TABLE `Sandelis` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `pavadinimas` varchar(255),
  `adresas` int,
  `plotas` double,
  `darbuotoju_sk` int,
  `talpa` double,
  `statybos_metai` date
);

CREATE TABLE `Inventorius` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `uzsakymo_data` datetime,
  `kiekis` int,
  `uzsake` int,
  `sandelis_id` int,
  `tipas` ENUM ('Paketai', 'Irankiai', 'Ofiso_iranga'),
  `komentaras` varchar(255),
  `yra_sandelyje` boolean
);

CREATE TABLE `Zymejimasis` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `data` datetime,
  `pasizymejimo_laikas` timestamp,
  `atsizymejimo_laikas` timestamp,
  `darbo_kiekis` int,
  `darbo_tipas` ENUM ('Siuntu registravimas', 'Siuntu isveziojimas'),
  `virsvalandziai` int
);

CREATE TABLE `Darb_zym` (
  `zymejimas_id` int,
  `darbuotojas_id` int
);

CREATE TABLE `Naudotojas` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `vardas` varchar(255),
  `pavarde` varchar(255),
  `gimimo_data` date,
  `el_pastas` varchar(255),
  `adresas` varchar(255),
  `pasto_kodas` varchar(255),
  `slapyvardis` varchar(255),
  `slaptazodis` varchar(255),
  `tipas` ENUM ('Klietas', 'Vadybininkas', 'Darbuotoas')
);

CREATE TABLE `Atsiliepimas` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `tekstas` varchar(255),
  `data` date,
  `ivertinimas` int,
  `antraste` varchar(255),
  `naudotojo_id` int,
  `uzsakymo_id` int
);

CREATE TABLE `Uzsakymas` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `sukurimo_data` date,
  `pristatymo_data` date,
  `patvirtinimo_data` date,
  `busena` ENUM ('uzsakyta', 'patvirtinta', 'pristatyta', 'atmesta'),
  `siuntinys_id` int,
  `naudotojo_id` int,
  `adresas` int
);

CREATE TABLE `Siuntinys` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `svoris` double,
  `aukstis` double,
  `plotis` double,
  `ilgis` double
);

CREATE TABLE `Turtas` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `pavadinimas` varchar(255),
  `tipas` ENUM ('Nekilnojamas turtas', 'Transportas', 'Darbo_iranga'),
  `verte` double,
  `isigyjimo_data` date,
  `kiekis` int
);

CREATE TABLE `Pavedimo_operacija` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `data` date,
  `pavedimo_tipas` ENUM ('Pardavimas', 'Pirkimas'),
  `turtas_id` int,
  `naudotojo_id` int
);

CREATE TABLE `Skola` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `tipas` varchar(255),
  `isdavimo_data` date,
  `grazinimo_data` date,
  `palukanos` double,
  `isdavejes` varchar(255),
  `dydis` double,
  `grazinta_suma` double,
  `naudotojo_id` int
);

CREATE TABLE `Finansinis_rodiklis` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `pavadinimas` varchar(255),
  `tipas` ENUM ('EBITDA', 'CAPEX', 'ROE', 'ROA', 'Gross_margin', 'Net_debt', 'Working_capital', 'P/E', 'P/B', 'Free_cash_flow'),
  `data` date,
  `rodiklio_reiksme` double,
  `naudotojo_id` int
);

CREATE TABLE `Apmokejimas` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `kiekis` int,
  `data` date
);
