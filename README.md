After cloning repository you have to download all needed packages:


`composer install`


To start server:


`symfony server:start`

or

`php -S 127.0.0.1:8000 -t public`


Ataskaita:

http://tiny.cc/z7hgcz



Kartais symfony mėgsta išmesti exceptioną, kad nerastas route arba tiesiog, kažko nėra projekte, tai tam reikia pravalyti cache.
**Kaip tai padaryti?**
Folderyje `/var` ištrinkite folderį `cache`.



**Kaip sukurti entity?**

https://symfony.com/doc/current/doctrine.html#creating-an-entity-class

**Kaip iš entity padaryti visą CRUD sistemą? CRUD - Create, read, update and delete**

php bin/console make:crud
https://symfony.com/blog/new-and-improved-generators-for-makerbundle

**Jeigu susikurete entity, poto cruda, ir tas entity turi kazkoki relation su kitu entity, tai galite gauti toki errora:**

`Catchable Fatal Error: Object of class Proxies\__CG__\App\Entity\UserType could not be converted to string`

Tuomet jums reikia nueiti į tą entity, kuriame truksta ToString() funkcijos ir ją tiesiog implementinti.
Taip kaip aš padariau UserType.php entityje.

`public function __toString(): string {
        return $this->getName();
    }`

