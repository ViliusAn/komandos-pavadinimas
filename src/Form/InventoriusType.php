<?php

namespace App\Form;

use App\Entity\Inventorius;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class InventoriusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kiekis')
            ->add('komentaras')
            ->add('yra_sandelyje')
            ->add('sandelis_id')
            ->add('tipas')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Inventorius::class,
        ]);
    }
}
