<?php

namespace App\Form;

use App\Entity\Turtas;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TurtasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {    
        $builder
            ->add('pavadinimas')
            ->add('verte')
            ->add('isigyjimo_data')
            ->add('kiekis')
            // ->add('tipas')
        ;
        // if ($options['ar_turtas']) {
        //     $builder->add('tipas');
        // }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Turtas::class,
            // 'ar_turtas' => false
        ]);
    }
}
