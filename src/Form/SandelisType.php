<?php

namespace App\Form;

use App\Entity\Sandelis;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SandelisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pavadinimas')
            ->add('adresas')
            ->add('plotas')
            ->add('darbuotoju_sk')
            ->add('talpa')
            ->add('statybos_metai')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sandelis::class,
        ]);
    }
}
