<?php

namespace App\Form;

use App\Entity\Siuntinys;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiuntinysType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('svoris')
            ->add('aukstis')
            ->add('plotis')
            ->add('ilgis')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Siuntinys::class,
        ]);
    }
}
