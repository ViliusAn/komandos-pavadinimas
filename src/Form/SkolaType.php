<?php

namespace App\Form;

use App\Entity\Skola;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SkolaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipas')
            ->add('isdavimo_data')
            ->add('grazinimo_data')
            ->add('palukanos')
            ->add('isdavejas')
            ->add('dydis')
            ->add('grazinta_suma')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Skola::class,
        ]);
    }
}
