<?php

namespace App\Form;

use App\Entity\Marsrutas;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class MarsrutasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isvykimas', DateTimeType::class, [
                    'data' => new \DateTime(),
                    'input' => 'datetime'
                ])
            ->add('atvykimas', DateTimeType::class, [
                'data' => new \DateTime(),
                'input' => 'datetime'
            ])
            ->add('sandelis_id')
            ->add('inventorius_id')
            ->add('kryptis')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Marsrutas::class,
        ]);
    }
}
