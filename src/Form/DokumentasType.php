<?php

namespace App\Form;

use App\Entity\Dokumentas;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DokumentasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pavadinimas')
            //->add('sudarymo_data')
            ->add('aprasymas')
            ->add('url_adresas')
            //->add('tipas')
            ->add('darbuotojas')

            ->add('Saugoti', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dokumentas::class,
        ]);
    }
}
