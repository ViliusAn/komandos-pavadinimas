<?php

namespace App\Form;

use App\Entity\Uzsakymas;
use App\Form\SiuntinysType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UzsakymasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pristatymo_data')
            ->add('adresas')
            ->add('siuntinys', SiuntinysType::class)
        ;

        if($options['showUser']) {
            $builder->add('user');
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Uzsakymas::class,
            'showUser' => null,
        ]);
    }
}
