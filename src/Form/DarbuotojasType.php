<?php

namespace App\Form;

use App\Entity\Darbuotojas;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DarbuotojasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('telefono_nr')
            ->add('banko_saskaitos_nr')
            ->add('sandelis_id')
            //->add('dokumentai')
            //->add('naudotojas')
            ->add('pareigos')

            ->add('Saugoti', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Darbuotojas::class,
        ]);
    }
}
