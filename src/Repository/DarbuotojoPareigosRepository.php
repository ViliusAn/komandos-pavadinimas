<?php

namespace App\Repository;

use App\Entity\DarbuotojoPareigos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DarbuotojoPareigos|null find($id, $lockMode = null, $lockVersion = null)
 * @method DarbuotojoPareigos|null findOneBy(array $criteria, array $orderBy = null)
 * @method DarbuotojoPareigos[]    findAll()
 * @method DarbuotojoPareigos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DarbuotojoPareigosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DarbuotojoPareigos::class);
    }

    // /**
    //  * @return DarbuotojoPareigos[] Returns an array of DarbuotojoPareigos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DarbuotojoPareigos
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
