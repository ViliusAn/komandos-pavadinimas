<?php

namespace App\Repository;

use App\Entity\Inventorius;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Inventorius|null find($id, $lockMode = null, $lockVersion = null)
 * @method Inventorius|null findOneBy(array $criteria, array $orderBy = null)
 * @method Inventorius[]    findAll()
 * @method Inventorius[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InventoriusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Inventorius::class);
    }

    // /**
    //  * @return Inventorius[] Returns an array of Inventorius objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Inventorius
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
