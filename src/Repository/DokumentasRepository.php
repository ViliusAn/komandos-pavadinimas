<?php

namespace App\Repository;

use App\Entity\Dokumentas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Dokumentas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dokumentas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dokumentas[]    findAll()
 * @method Dokumentas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DokumentasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dokumentas::class);
    }

    // /**
    //  * @return Dokumentas[] Returns an array of Dokumentas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dokumentas
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
