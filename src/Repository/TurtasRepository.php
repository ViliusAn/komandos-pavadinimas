<?php

namespace App\Repository;

use App\Entity\Turtas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Turtas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Turtas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Turtas[]    findAll()
 * @method Turtas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TurtasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Turtas::class);
    }

    public function getAllEquityByPeriod($from, $to) : array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT * FROM turtas t
            WHERE t.isigyjimo_data > :from AND
            t.isigyjimo_data < :to
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['from' => $from, 'to' => $to]);
    
        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    public function getAllEquity() : array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM turtas t';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    
        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }
    
    // /**
    //  * @return Turtas[] Returns an array of Turtas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Turtas
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
