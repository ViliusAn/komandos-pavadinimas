<?php

namespace App\Repository;

use App\Entity\FinansinisRodiklis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FinansinisRodiklis|null find($id, $lockMode = null, $lockVersion = null)
 * @method FinansinisRodiklis|null findOneBy(array $criteria, array $orderBy = null)
 * @method FinansinisRodiklis[]    findAll()
 * @method FinansinisRodiklis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FinansinisRodiklisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FinansinisRodiklis::class);
    }

    // /**
    //  * @return FinansinisRodiklis[] Returns an array of FinansinisRodiklis objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FinansinisRodiklis
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
