<?php

namespace App\Repository;

use App\Entity\InventoriausTipas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method InventoriausTipas|null find($id, $lockMode = null, $lockVersion = null)
 * @method InventoriausTipas|null findOneBy(array $criteria, array $orderBy = null)
 * @method InventoriausTipas[]    findAll()
 * @method InventoriausTipas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InventoriausTipasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InventoriausTipas::class);
    }

    // /**
    //  * @return InventoriausTipas[] Returns an array of InventoriausTipas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InventoriausTipas
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
