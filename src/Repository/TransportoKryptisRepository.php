<?php

namespace App\Repository;

use App\Entity\TransportoKryptis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TransportoKryptis|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransportoKryptis|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransportoKryptis[]    findAll()
 * @method TransportoKryptis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransportoKryptisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransportoKryptis::class);
    }

    // /**
    //  * @return TransportoKryptis[] Returns an array of TransportoKryptis objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TransportoKryptis
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
