<?php

namespace App\Repository;

use App\Entity\DokumentoTipas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DokumentoTipas|null find($id, $lockMode = null, $lockVersion = null)
 * @method DokumentoTipas|null findOneBy(array $criteria, array $orderBy = null)
 * @method DokumentoTipas[]    findAll()
 * @method DokumentoTipas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DokumentoTipasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DokumentoTipas::class);
    }

    // /**
    //  * @return DokumentoTipas[] Returns an array of DokumentoTipas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DokumentoTipas
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
