<?php

namespace App\Repository;

use App\Entity\Atostogos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Atostogos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Atostogos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Atostogos[]    findAll()
 * @method Atostogos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AtostogosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Atostogos::class);
    }

    // /**
    //  * @return Atostogos[] Returns an array of Atostogos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Atostogos
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
