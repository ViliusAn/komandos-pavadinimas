<?php

namespace App\Repository;

use App\Entity\PavedimoTipas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PavedimoTipas|null find($id, $lockMode = null, $lockVersion = null)
 * @method PavedimoTipas|null findOneBy(array $criteria, array $orderBy = null)
 * @method PavedimoTipas[]    findAll()
 * @method PavedimoTipas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PavedimoTipasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PavedimoTipas::class);
    }

    // /**
    //  * @return PavedimoTipas[] Returns an array of PavedimoTipas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PavedimoTipas
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
