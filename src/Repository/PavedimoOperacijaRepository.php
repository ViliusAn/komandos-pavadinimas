<?php

namespace App\Repository;

use App\Entity\PavedimoOperacija;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PavedimoOperacija|null find($id, $lockMode = null, $lockVersion = null)
 * @method PavedimoOperacija|null findOneBy(array $criteria, array $orderBy = null)
 * @method PavedimoOperacija[]    findAll()
 * @method PavedimoOperacija[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PavedimoOperacijaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PavedimoOperacija::class);
    }

    // /**
    //  * @return PavedimoOperacija[] Returns an array of PavedimoOperacija objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PavedimoOperacija
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
