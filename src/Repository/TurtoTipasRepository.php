<?php

namespace App\Repository;

use App\Entity\TurtoTipas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TurtoTipas|null find($id, $lockMode = null, $lockVersion = null)
 * @method TurtoTipas|null findOneBy(array $criteria, array $orderBy = null)
 * @method TurtoTipas[]    findAll()
 * @method TurtoTipas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TurtoTipasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TurtoTipas::class);
    }

    // /**
    //  * @return TurtoTipas[] Returns an array of TurtoTipas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TurtoTipas
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
