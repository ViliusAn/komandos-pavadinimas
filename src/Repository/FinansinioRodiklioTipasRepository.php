<?php

namespace App\Repository;

use App\Entity\FinansinioRodiklioTipas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FinansinioRodiklioTipas|null find($id, $lockMode = null, $lockVersion = null)
 * @method FinansinioRodiklioTipas|null findOneBy(array $criteria, array $orderBy = null)
 * @method FinansinioRodiklioTipas[]    findAll()
 * @method FinansinioRodiklioTipas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FinansinioRodiklioTipasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FinansinioRodiklioTipas::class);
    }

    // /**
    //  * @return FinansinioRodiklioTipas[] Returns an array of FinansinioRodiklioTipas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FinansinioRodiklioTipas
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
