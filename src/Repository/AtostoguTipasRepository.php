<?php

namespace App\Repository;

use App\Entity\AtostoguTipas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AtostoguTipas|null find($id, $lockMode = null, $lockVersion = null)
 * @method AtostoguTipas|null findOneBy(array $criteria, array $orderBy = null)
 * @method AtostoguTipas[]    findAll()
 * @method AtostoguTipas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AtostoguTipasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AtostoguTipas::class);
    }

    // /**
    //  * @return AtostoguTipas[] Returns an array of AtostoguTipas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AtostoguTipas
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
