<?php

namespace App\Repository;

use App\Entity\DarboTipas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DarboTipas|null find($id, $lockMode = null, $lockVersion = null)
 * @method DarboTipas|null findOneBy(array $criteria, array $orderBy = null)
 * @method DarboTipas[]    findAll()
 * @method DarboTipas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DarboTipasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DarboTipas::class);
    }

    // /**
    //  * @return DarboTipas[] Returns an array of DarboTipas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DarboTipas
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
