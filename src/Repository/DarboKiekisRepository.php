<?php

namespace App\Repository;

use App\Entity\DarboKiekis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DarboKiekis|null find($id, $lockMode = null, $lockVersion = null)
 * @method DarboKiekis|null findOneBy(array $criteria, array $orderBy = null)
 * @method DarboKiekis[]    findAll()
 * @method DarboKiekis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DarboKiekisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DarboKiekis::class);
    }

    // /**
    //  * @return DarboKiekis[] Returns an array of DarboKiekis objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DarboKiekis
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
