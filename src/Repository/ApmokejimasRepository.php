<?php

namespace App\Repository;

use App\Entity\Apmokejimas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Apmokejimas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Apmokejimas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Apmokejimas[]    findAll()
 * @method Apmokejimas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApmokejimasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Apmokejimas::class);
    }

    public function getAllPayments($from, $to) : array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT * FROM apmokejimas a
            WHERE a.data > :from AND
            a.data < :to
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['from' => $from, 'to' => $to]);
    
        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    // /**
    //  * @return Apmokejimas[] Returns an array of Apmokejimas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Apmokejimas
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
