<?php

namespace App\Repository;

use App\Entity\Virsvalandziai;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Virsvalandziai|null find($id, $lockMode = null, $lockVersion = null)
 * @method Virsvalandziai|null findOneBy(array $criteria, array $orderBy = null)
 * @method Virsvalandziai[]    findAll()
 * @method Virsvalandziai[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VirsvalandziaiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Virsvalandziai::class);
    }

    // /**
    //  * @return Virsvalandziai[] Returns an array of Virsvalandziai objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Virsvalandziai
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
