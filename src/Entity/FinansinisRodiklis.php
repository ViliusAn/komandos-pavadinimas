<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FinansinisRodiklisRepository")
 */
class FinansinisRodiklis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Pavadinimas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FinansinioRodiklioTipas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Tipas;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Data;

    /**
     * @ORM\Column(type="float")
     */
    private $Rodiklio_reiksme;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->Pavadinimas;
    }

    public function setPavadinimas(string $Pavadinimas): self
    {
        $this->Pavadinimas = $Pavadinimas;

        return $this;
    }

    public function getTipas(): ?FinansinioRodiklioTipas
    {
        return $this->Tipas;
    }

    public function setTipas(?FinansinioRodiklioTipas $Tipas): self
    {
        $this->Tipas = $Tipas;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->Data;
    }

    public function setData(\DateTimeInterface $Data): self
    {
        $this->Data = $Data;

        return $this;
    }

    public function getRodiklioReiksme(): ?float
    {
        return $this->Rodiklio_reiksme;
    }

    public function setRodiklioReiksme(float $Rodiklio_reiksme): self
    {
        $this->Rodiklio_reiksme = $Rodiklio_reiksme;

        return $this;
    }
}
