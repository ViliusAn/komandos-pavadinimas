<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AtsiliepimasRepository")
 */
class Atsiliepimas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $tekstas;

    /**
     * @ORM\Column(type="datetime")
     */
    private $data;

    /**
     * @ORM\Column(type="text")
     */
    private $antraste;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Uzsakymas", inversedBy="atsiliepimas")
     */
    private $uzsakymas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="atsiliepimas")
     */
    private $user;

    public function __construct()
    {
        $this->data = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTekstas(): ?string
    {
        return $this->tekstas;
    }

    public function setTekstas(string $tekstas): self
    {
        $this->tekstas = $tekstas;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getAntraste(): ?string
    {
        return $this->antraste;
    }

    public function setAntraste(string $antraste): self
    {
        $this->antraste = $antraste;

        return $this;
    }

    public function getUzsakymas(): ?Uzsakymas
    {
        return $this->uzsakymas;
    }

    public function setUzsakymas(?Uzsakymas $uzsakymas): self
    {
        $this->uzsakymas = $uzsakymas;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
