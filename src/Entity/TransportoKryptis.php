<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransportoKryptisRepository")
 */
class TransportoKryptis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $verte;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVerte(): ?string
    {
        return $this->verte;
    }

    public function setVerte(string $verte): self
    {
        $this->verte = $verte;

        return $this;
    }
    public function __toString(): string {
        return $this->getVerte();
    }
}
