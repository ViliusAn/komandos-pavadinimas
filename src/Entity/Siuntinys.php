<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SiuntinysRepository")
 */
class Siuntinys
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $svoris;

    /**
     * @ORM\Column(type="float")
     */
    private $aukstis;

    /**
     * @ORM\Column(type="float")
     */
    private $plotis;

    /**
     * @ORM\Column(type="float")
     */
    private $ilgis;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSvoris(): ?float
    {
        return $this->svoris;
    }

    public function setSvoris(float $svoris): self
    {
        $this->svoris = $svoris;

        return $this;
    }

    public function getAukstis(): ?float
    {
        return $this->aukstis;
    }

    public function setAukstis(float $aukstis): self
    {
        $this->aukstis = $aukstis;

        return $this;
    }

    public function getPlotis(): ?float
    {
        return $this->plotis;
    }

    public function setPlotis(float $plotis): self
    {
        $this->plotis = $plotis;

        return $this;
    }

    public function getIlgis(): ?float
    {
        return $this->ilgis;
    }

    public function setIlgis(float $ilgis): self
    {
        $this->ilgis = $ilgis;

        return $this;
    }
}
