<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DarboKiekisRepository")
 */
class DarboKiekis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DarboTipas", inversedBy="darboKiekis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tipas;

    /**
     * @ORM\Column(type="integer")
     */
    private $valandu_kiekis;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Zymejimasis", inversedBy="darbo_kiekis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $zymejimasis;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipas(): ?DarboTipas
    {
        return $this->tipas;
    }

    public function setTipas(?DarboTipas $tipas): self
    {
        $this->tipas = $tipas;

        return $this;
    }

    public function getValanduKiekis(): ?int
    {
        return $this->valandu_kiekis;
    }

    public function setValanduKiekis(int $valandu_kiekis): self
    {
        $this->valandu_kiekis = $valandu_kiekis;

        return $this;
    }

    public function getZymejimasis(): ?Zymejimasis
    {
        return $this->zymejimasis;
    }

    public function setZymejimasis(?Zymejimasis $zymejimasis): self
    {
        $this->zymejimasis = $zymejimasis;

        return $this;
    }
}
