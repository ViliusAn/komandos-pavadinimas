<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApmokejimasRepository")
 */
class Apmokejimas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Kiekis;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Data;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\user", inversedBy="kliento_id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Kliento_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKiekis(): ?int
    {
        return $this->Kiekis;
    }

    public function setKiekis(int $Kiekis): self
    {
        $this->Kiekis = $Kiekis;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->Data;
    }

    public function setData(\DateTimeInterface $Data): self
    {
        $this->Data = $Data;

        return $this;
    }

    public function getKlientoId(): ?user
    {
        return $this->Kliento_id;
    }

    public function setKlientoId(?user $Kliento_id): self
    {
        $this->Kliento_id = $Kliento_id;

        return $this;
    }
}
