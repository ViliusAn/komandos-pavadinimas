<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ZymejimasisRepository")
 */
class Zymejimasis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $data;

    /**
     * @ORM\Column(type="time")
     */
    private $pasizymejimo_laikas;

    /**
     * @ORM\Column(type="integer")
     */
    private $isdirbtas_laikas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Darbuotojas", inversedBy="pasizymejimas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $darbuotojas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DarboKiekis", mappedBy="zymejimasis")
     */
    private $darbo_kiekis;

    public function __construct()
    {
        $this->darbo_kiekis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getPasizymejimoLaikas(): ?\DateTimeInterface
    {
        return $this->pasizymejimo_laikas;
    }

    public function setPasizymejimoLaikas(\DateTimeInterface $pasizymejimo_laikas): self
    {
        $this->pasizymejimo_laikas = $pasizymejimo_laikas;

        return $this;
    }

    public function getIsdirbtasLaikas(): ?int
    {
        return $this->isdirbtas_laikas;
    }

    public function setIsdirbtasLaikas(int $isdirbtas_laikas): self
    {
        $this->isdirbtas_laikas = $isdirbtas_laikas;

        return $this;
    }

    public function getDarbuotojas(): ?Darbuotojas
    {
        return $this->darbuotojas;
    }

    public function setDarbuotojas(?Darbuotojas $darbuotojas): self
    {
        $this->darbuotojas = $darbuotojas;

        return $this;
    }

    /**
     * @return Collection|DarboKiekis[]
     */
    public function getDarboKiekis(): Collection
    {
        return $this->darbo_kiekis;
    }

    public function addDarboKieki(DarboKiekis $darboKieki): self
    {
        if (!$this->darbo_kiekis->contains($darboKieki)) {
            $this->darbo_kiekis[] = $darboKieki;
            $darboKieki->setZymejimasis($this);
        }

        return $this;
    }

    public function removeDarboKieki(DarboKiekis $darboKieki): self
    {
        if ($this->darbo_kiekis->contains($darboKieki)) {
            $this->darbo_kiekis->removeElement($darboKieki);
            // set the owning side to null (unless already changed)
            if ($darboKieki->getZymejimasis() === $this) {
                $darboKieki->setZymejimasis(null);
            }
        }

        return $this;
    }
}
