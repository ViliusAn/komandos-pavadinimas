<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SkolaRepository")
 */
class Skola
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipas;

    /**
     * @ORM\Column(type="datetime")
     */
    private $isdavimo_data;

    /**
     * @ORM\Column(type="datetime")
     */
    private $grazinimo_data;

    /**
     * @ORM\Column(type="float")
     */
    private $palukanos;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $isdavejas;

    /**
     * @ORM\Column(type="integer")
     */
    private $dydis;

    /**
     * @ORM\Column(type="integer")
     */
    private $grazinta_suma;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipas(): ?string
    {
        return $this->tipas;
    }

    public function setTipas(string $tipas): self
    {
        $this->tipas = $tipas;

        return $this;
    }

    public function getIsdavimoData(): ?\DateTimeInterface
    {
        return $this->isdavimo_data;
    }

    public function setIsdavimoData(\DateTimeInterface $isdavimo_data): self
    {
        $this->isdavimo_data = $isdavimo_data;

        return $this;
    }

    public function getGrazinimoData(): ?\DateTimeInterface
    {
        return $this->grazinimo_data;
    }

    public function setGrazinimoData(\DateTimeInterface $grazinimo_data): self
    {
        $this->grazinimo_data = $grazinimo_data;

        return $this;
    }

    public function getPalukanos(): ?float
    {
        return $this->palukanos;
    }

    public function setPalukanos(float $palukanos): self
    {
        $this->palukanos = $palukanos;

        return $this;
    }

    public function getIsdavejas(): ?string
    {
        return $this->isdavejas;
    }

    public function setIsdavejas(string $isdavejas): self
    {
        $this->isdavejas = $isdavejas;

        return $this;
    }

    public function getDydis(): ?int
    {
        return $this->dydis;
    }

    public function setDydis(int $dydis): self
    {
        $this->dydis = $dydis;

        return $this;
    }

    public function getGrazintaSuma(): ?int
    {
        return $this->grazinta_suma;
    }

    public function setGrazintaSuma(int $grazinta_suma): self
    {
        $this->grazinta_suma = $grazinta_suma;

        return $this;
    }
}
