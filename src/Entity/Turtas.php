<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TurtasRepository")
 */
class Turtas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TurtoTipas", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $tipas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PavedimoOperacija", mappedBy="turtas", orphanRemoval=true)
     */
    private $pavedimo_operacija;

    /**
     * @ORM\Column(type="float")
     */
    private $verte;

    /**
     * @ORM\Column(type="datetime")
     */
    private $isigyjimo_data;

    /**
     * @ORM\Column(type="integer")
     */
    private $kiekis;

    public function __construct()
    {
        $this->pavedimo_operacija = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    public function getTipas(): ?TurtoTipas
    {
        return $this->tipas;
    }

    public function setTipas(TurtoTipas $tipas): self
    {
        $this->tipas = $tipas;

        return $this;
    }

    /**
     * @return Collection|PavedimoOperacija[]
     */
    public function getPavedimoOperacija(): Collection
    {
        return $this->pavedimo_operacija;
    }

    public function addPavedimoOperacija(PavedimoOperacija $pavedimoOperacija): self
    {
        if (!$this->pavedimo_operacija->contains($pavedimoOperacija)) {
            $this->pavedimo_operacija[] = $pavedimoOperacija;
            $pavedimoOperacija->setTurtas($this);
        }

        return $this;
    }

    public function removePavedimoOperacija(PavedimoOperacija $pavedimoOperacija): self
    {
        if ($this->pavedimo_operacija->contains($pavedimoOperacija)) {
            $this->pavedimo_operacija->removeElement($pavedimoOperacija);
            // set the owning side to null (unless already changed)
            if ($pavedimoOperacija->getTurtas() === $this) {
                $pavedimoOperacija->setTurtas(null);
            }
        }

        return $this;
    }

    public function getVerte(): ?float
    {
        return $this->verte;
    }

    public function setVerte(float $verte): self
    {
        $this->verte = $verte;

        return $this;
    }

    public function getIsigyjimoData(): ?\DateTimeInterface
    {
        return $this->isigyjimo_data;
    }

    public function setIsigyjimoData(\DateTimeInterface $isigyjimo_data): self
    {
        $this->isigyjimo_data = $isigyjimo_data;

        return $this;
    }

    public function getKiekis(): ?int
    {
        return $this->kiekis;
    }

    public function setKiekis(int $kiekis): self
    {
        $this->kiekis = $kiekis;

        return $this;
    }

}
