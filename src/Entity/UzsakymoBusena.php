<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UzsakymoBusenaRepository")
 */
class UzsakymoBusena
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Uzsakymas", mappedBy="busena")
     */
    private $uzsakymai;

    public function __construct()
    {
        $this->uzsakymai = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    /**
     * @return Collection|Uzsakymas[]
     */
    public function getUzsakymai(): Collection
    {
        return $this->uzsakymai;
    }

    public function addUzsakymas(Uzsakymas $uzsakymas): self
    {
        if (!$this->uzsakymai->contains($uzsakymas)) {
            $this->uzsakymai[] = $uzsakymas;
            $uzsakymas->setBusena($this);
        }

        return $this;
    }

    public function removeUzsakymas(Uzsakymas $uzsakymas): self
    {
        if ($this->uzsakymai->contains($uzsakymas)) {
            $this->uzsakymai->removeElement($uzsakymas);
            // set the owning side to null (unless already changed)
            if ($uzsakymas->getBusena() === $this) {
                $uzsakymas->setBusena(null);
            }
        }

        return $this;
    }

    public function __toString(): string {
        return $this->getPavadinimas();
    }
}
