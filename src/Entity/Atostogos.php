<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AtostogosRepository")
 */
class Atostogos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $pradzios_data;

    /**
     * @ORM\Column(type="date")
     */
    private $pabaigos_data;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AtostoguTipas", inversedBy="atostogos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tipas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Darbuotojas", inversedBy="atostogos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $darbuotojas;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPradziosData(): ?\DateTimeInterface
    {
        return $this->pradzios_data;
    }

    public function setPradziosData(\DateTimeInterface $pradzios_data): self
    {
        $this->pradzios_data = $pradzios_data;

        return $this;
    }

    public function getPabaigosData(): ?\DateTimeInterface
    {
        return $this->pabaigos_data;
    }

    public function setPabaigosData(\DateTimeInterface $pabaigos_data): self
    {
        $this->pabaigos_data = $pabaigos_data;

        return $this;
    }

    public function getTipas(): ?AtostoguTipas
    {
        return $this->tipas;
    }

    public function setTipas(?AtostoguTipas $tipas): self
    {
        $this->tipas = $tipas;

        return $this;
    }

    public function getDarbuotojas(): ?Darbuotojas
    {
        return $this->darbuotojas;
    }

    public function setDarbuotojas(?Darbuotojas $darbuotojas): self
    {
        $this->darbuotojas = $darbuotojas;

        return $this;
    }
}
