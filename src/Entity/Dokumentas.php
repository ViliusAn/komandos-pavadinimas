<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DokumentasRepository")
 */
class Dokumentas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;


    /**
     * @ORM\Column(type="datetime")
     */
    private $sudarymo_data;

    /**
     * @ORM\Column(type="string", length=5000)
     */
    private $aprasymas;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $url_adresas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DokumentoTipas", inversedBy="dokumentai")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tipas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Darbuotojas", inversedBy="dokumentai")
     * @ORM\JoinColumn(nullable=false)
     */
    private $darbuotojas;

    public function __construct()
    {
        $this->darbuotojai = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    public function getSudarymoData(): ?\DateTimeInterface
    {
        return $this->sudarymo_data;
    }

    public function setSudarymoData(\DateTimeInterface $sudarymo_data): self
    {
        $this->sudarymo_data = $sudarymo_data;

        return $this;
    }

    public function getAprasymas(): ?string
    {
        return $this->aprasymas;
    }

    public function setAprasymas(string $aprasymas): self
    {
        $this->aprasymas = $aprasymas;

        return $this;
    }

    public function getUrlAdresas(): ?string
    {
        return $this->url_adresas;
    }

    public function setUrlAdresas(string $url_adresas): self
    {
        $this->url_adresas = $url_adresas;

        return $this;
    }

    public function getTipas(): ?DokumentoTipas
    {
        return $this->tipas;
    }

    public function setTipas(?DokumentoTipas $tipas): self
    {
        $this->tipas = $tipas;

        return $this;
    }

    public function getDarbuotojas(): ?Darbuotojas
    {
        return $this->darbuotojas;
    }

    public function setDarbuotojas(?Darbuotojas $darbuotojas): self
    {
        $this->darbuotojas = $darbuotojas;

        return $this;
    }
}
