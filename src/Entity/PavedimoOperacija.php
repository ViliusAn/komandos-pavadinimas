<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PavedimoOperacijaRepository")
 */
class PavedimoOperacija
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $data;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PavedimoTipas", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $pavedimo_tipas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Turtas", inversedBy="pavedimo_operacija")
     * @ORM\JoinColumn(nullable=false)
     */
    private $turtas;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getPavedimoTipas(): ?PavedimoTipas
    {
        return $this->pavedimo_tipas;
    }

    public function setPavedimoTipas(PavedimoTipas $pavedimo_tipas): self
    {
        $this->pavedimo_tipas = $pavedimo_tipas;

        return $this;
    }

    public function getTurtas(): ?Turtas
    {
        return $this->turtas;
    }

    public function setTurtas(?Turtas $turtas): self
    {
        $this->turtas = $turtas;

        return $this;
    }
}
