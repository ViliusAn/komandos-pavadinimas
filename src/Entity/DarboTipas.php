<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DarboTipasRepository")
 */
class DarboTipas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DarboKiekis", mappedBy="tipas")
     */
    private $darboKiekis;

    public function __construct()
    {
        $this->darboKiekis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    /**
     * @return Collection|DarboKiekis[]
     */
    public function getDarboKiekis(): Collection
    {
        return $this->darboKiekis;
    }

    public function addDarboKieki(DarboKiekis $darboKieki): self
    {
        if (!$this->darboKiekis->contains($darboKieki)) {
            $this->darboKiekis[] = $darboKieki;
            $darboKieki->setTipas($this);
        }

        return $this;
    }

    public function removeDarboKieki(DarboKiekis $darboKieki): self
    {
        if ($this->darboKiekis->contains($darboKieki)) {
            $this->darboKiekis->removeElement($darboKieki);
            // set the owning side to null (unless already changed)
            if ($darboKieki->getTipas() === $this) {
                $darboKieki->setTipas(null);
            }
        }

        return $this;
    }
}
