<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserType", inversedBy="users")
     */
    private $userType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $vardas;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavarde;

    /**
     * @ORM\Column(type="date")
     */
    private $gimimo_data;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pasto_kodas;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Uzsakymas", mappedBy="user")
     */
    private $uzsakymai;

    /** 
     * @ORM\OneToMany(targetEntity="App\Entity\Apmokejimas", mappedBy="Kliento_id")
     */
    private $kliento_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Atsiliepimas", mappedBy="user")
     */
    private $atsiliepimai;

    public function __construct()
    {
        $this->uzsakymai = new ArrayCollection();
        $this->kliento_id = new ArrayCollection();
        $this->atsiliepimai = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return $this->getUserType()->getRoles();
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserType(): ?UserType
    {
        return $this->userType;
    }

    public function setUserType(?UserType $userType): self
    {
        $this->userType = $userType;

        return $this;
    }

    public function getVardas(): ?string
    {
        return $this->vardas;
    }

    public function setVardas(string $vardas): self
    {
        $this->vardas = $vardas;

        return $this;
    }

    public function getPavarde(): ?string
    {
        return $this->pavarde;
    }

    public function setPavarde(string $pavarde): self
    {
        $this->pavarde = $pavarde;

        return $this;
    }

    public function getGimimoData(): ?\DateTimeInterface
    {
        return $this->gimimo_data;
    }

    public function setGimimoData(\DateTimeInterface $gimimo_data): self
    {
        $this->gimimo_data = $gimimo_data;

        return $this;
    }

    public function getPastoKodas(): ?string
    {
        return $this->pasto_kodas;
    }

    public function setPastoKodas(string $pasto_kodas): self
    {
        $this->pasto_kodas = $pasto_kodas;

        return $this;
    }

    public function getAdresas(): ?string
    {
        return $this->adresas;
    }

    public function setAdresas(string $adresas): self
    {
        $this->adresas = $adresas;

        return $this;
    }

    /**
     * @return Collection|Uzsakymas[]
     */
    public function getUzsakymai(): Collection
    {
        return $this->uzsakymai;
    }

    public function addUzsakyma(Uzsakymas $uzsakymas): self
    {
        if (!$this->uzsakymai->contains($uzsakymas)) {
            $this->uzsakymai[] = $uzsakymas;
            $uzsakymas->setUser($this);
        }

        return $this;
    }

    public function removeUzsakymas(Uzsakymas $uzsakymas): self
    {
        if ($this->uzsakymai->contains($uzsakymas)) {
            $this->uzsakymai->removeElement($uzsakymas);
            // set the owning side to null (unless already changed)
            if ($uzsakymas->getUser() === $this) {
                $uzsakymas->setUser(null);
            }
        }

        return $this;
    }

    public function __toString(): string {
        $string = $this->getVardas() . " " . $this->getPavarde();
        return $string;
    }

    /**
     * @return Collection|Apmokejimas[]
     */
    public function getKlientoId(): Collection
    {
        return $this->kliento_id;
    }

    public function addKlientoId(Apmokejimas $klientoId): self
    {
        if (!$this->kliento_id->contains($klientoId)) {
            $this->kliento_id[] = $klientoId;
            $klientoId->setKlientoId($this);
        }

        return $this;
    }

    public function removeKlientoId(Apmokejimas $klientoId): self
    {
        if ($this->kliento_id->contains($klientoId)) {
            $this->kliento_id->removeElement($klientoId);
            // set the owning side to null (unless already changed)
            if ($klientoId->getKlientoId() === $this) {
                $klientoId->setKlientoId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Atsiliepimas[]
     */
    public function getAtsiliepimas(): Collection
    {
        return $this->atsiliepimas;
    }

    public function addAtsiliepimas(Atsiliepimas $atsiliepimas): self
    {
        if (!$this->atsiliepimas->contains($atsiliepimas)) {
            $this->atsiliepimas[] = $atsiliepimas;
            $atsiliepimas->setUser($this);
        }

        return $this;
    }

    public function removeAtsiliepimas(Atsiliepimas $atsiliepimas): self
    {
        if ($this->atsiliepimas->contains($atsiliepimas)) {
            $this->atsiliepimas->removeElement($atsiliepimas);
            // set the owning side to null (unless already changed)
            if ($atsiliepimas->getUser() === $this) {
                $atsiliepimas->setUser(null);
            }
        }

        return $this;
    }
}
