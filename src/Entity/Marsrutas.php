<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarsrutasRepository")
 */
class Marsrutas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sandelis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sandelis_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Inventorius")
     * @ORM\JoinColumn(nullable=false)
     */
    private $inventorius_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TransportoKryptis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $kryptis;

    /**
     * @ORM\Column(type="datetime")
     */
    private $atvykimas;

    /**
     * @ORM\Column(type="datetime")
     */
    private $isvykimas;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSandelisId(): ?Sandelis
    {
        return $this->sandelis_id;
    }

    public function setSandelisId(?Sandelis $sandelis_id): self
    {
        $this->sandelis_id = $sandelis_id;

        return $this;
    }

    public function getInventoriusId(): ?Inventorius
    {
        return $this->inventorius_id;
    }

    public function setInventoriusId(?Inventorius $inventorius_id): self
    {
        $this->inventorius_id = $inventorius_id;

        return $this;
    }

    public function getKryptis(): ?TransportoKryptis
    {
        return $this->kryptis;
    }

    public function setKryptis(?TransportoKryptis $kryptis): self
    {
        $this->kryptis = $kryptis;

        return $this;
    }

    public function getAtvykimas(): ?\DateTimeInterface
    {
        return $this->atvykimas;
    }

    public function setAtvykimas(\DateTimeInterface $atvykimas): self
    {
        $this->atvykimas = $atvykimas;

        return $this;
    }

    public function getIsvykimas(): ?\DateTimeInterface
    {
        return $this->isvykimas;
    }

    public function setIsvykimas(\DateTimeInterface $isvykimas): self
    {
        $this->isvykimas = $isvykimas;

        return $this;
    }

    public function getTrukme()
    {
        $isvykimas = $this->getIsvykimas();
        $atvykimas = $this->getAtvykimas();
        $skirtumas = date_diff($atvykimas ,$isvykimas);
        $skirtumas = $skirtumas->format('%d d, %H val, %m min');
        return $skirtumas ;
    }
}
