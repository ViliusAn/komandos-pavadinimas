<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VirsvalandziaiRepository")
 */
class Virsvalandziai
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $valandu_kiekis;

    /**
     * @ORM\Column(type="datetime")
     */
    private $registravimo_data;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Darbuotojas", inversedBy="virsvalandziai")
     * @ORM\JoinColumn(nullable=false)
     */
    private $darbuotojas;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValanduKiekis(): ?int
    {
        return $this->valandu_kiekis;
    }

    public function setValanduKiekis(int $valandu_kiekis): self
    {
        $this->valandu_kiekis = $valandu_kiekis;

        return $this;
    }

    public function getRegistravimoDataString(): string {
        $string = $this->getRegistravimoData();
        return $string;
    }

    public function getRegistravimoData(): ?\DateTimeInterface
    {
        return $this->registravimo_data;
    }

    public function setRegistravimoData(\DateTimeInterface $registravimo_data): self
    {
        $this->registravimo_data = $registravimo_data;

        return $this;
    }

    public function getDarbuotojas(): ?Darbuotojas
    {
        return $this->darbuotojas;
    }

    public function setDarbuotojas(?Darbuotojas $darbuotojas): self
    {
        $this->darbuotojas = $darbuotojas;

        return $this;
    }
}
