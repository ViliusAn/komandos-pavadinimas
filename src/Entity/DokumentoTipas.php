<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DokumentoTipasRepository")
 */
class DokumentoTipas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Dokumentas", mappedBy="tipas")
     */
    private $dokumentai;

    public function __toString(): string {
        $string = $this->getPavadinimas();
        return $string;
    }

    public function __construct()
    {
        $this->darbuotojai = new ArrayCollection();
        $this->dokumentai = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }
    
    /**
     * @return Collection|Dokumentas[]
     */
    public function getDokumentai(): Collection
    {
        return $this->dokumentai;
    }

    public function addDokumentai(Dokumentas $dokumentai): self
    {
        if (!$this->dokumentai->contains($dokumentai)) {
            $this->dokumentai[] = $dokumentai;
            $dokumentai->setTipas($this);
        }

        return $this;
    }

    public function removeDokumentai(Dokumentas $dokumentai): self
    {
        if ($this->dokumentai->contains($dokumentai)) {
            $this->dokumentai->removeElement($dokumentai);
            // set the owning side to null (unless already changed)
            if ($dokumentai->getTipas() === $this) {
                $dokumentai->setTipas(null);
            }
        }

        return $this;
    }
}
