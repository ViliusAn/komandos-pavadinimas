<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SandelisRepository")
 */
class Sandelis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;

    /**
     * @ORM\Column(type="string")
     */
    private $adresas;

    /**
     * @ORM\Column(type="float")
     */
    private $plotas;

    /**
     * @ORM\Column(type="integer")
     */
    private $darbuotoju_sk;

    /**
     * @ORM\Column(type="float")
     */
    private $talpa;

    /**
     * @ORM\Column(type="date")
     */
    private $statybos_metai;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Darbuotojas", mappedBy="sandelis_id")
     */
    private $darbuotojai;

    public function __construct()
    {
        $this->darbuotojai = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    public function getAdresas(): ?string
    {
        return $this->adresas;
    }

    public function setAdresas(string $adresas): self
    {
        $this->adresas = $adresas;

        return $this;
    }

    public function getPlotas(): ?float
    {
        return $this->plotas;
    }

    public function setPlotas(float $plotas): self
    {
        $this->plotas = $plotas;

        return $this;
    }

    public function getDarbuotojuSk(): ?int
    {
        return $this->darbuotoju_sk;
    }

    public function setDarbuotojuSk(int $darbuotoju_sk): self
    {
        $this->darbuotoju_sk = $darbuotoju_sk;

        return $this;
    }

    public function getTalpa(): ?float
    {
        return $this->talpa;
    }

    public function setTalpa(float $talpa): self
    {
        $this->talpa = $talpa;

        return $this;
    }

    public function getStatybosMetai(): ?\DateTimeInterface
    {
        return $this->statybos_metai;
    }

    public function setStatybosMetai(\DateTimeInterface $statybos_metai): self
    {
        $this->statybos_metai = $statybos_metai;

        return $this;
    }

    /**
     * @return Collection|Darbuotojas[]
     */
    public function getDarbuotojai(): Collection
    {
        return $this->darbuotojai;
    }

    public function addDarbuotojai(Darbuotojas $darbuotojai): self
    {
        if (!$this->darbuotojai->contains($darbuotojai)) {
            $this->darbuotojai[] = $darbuotojai;
            $darbuotojai->setSandelisId($this);
        }

        return $this;
    }

    public function removeDarbuotojai(Darbuotojas $darbuotojai): self
    {
        if ($this->darbuotojai->contains($darbuotojai)) {
            $this->darbuotojai->removeElement($darbuotojai);
            // set the owning side to null (unless already changed)
            if ($darbuotojai->getSandelisId() === $this) {
                $darbuotojai->setSandelisId(null);
            }
        }

        return $this;
    }

    public function __toString(): string {
        return $this->getPavadinimas();
    }
}
