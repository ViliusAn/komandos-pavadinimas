<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AtostoguTipasRepository")
 */
class AtostoguTipas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Atostogos", mappedBy="tipas")
     */
    private $atostogos;

    public function __construct()
    {
        $this->atostogos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    /**
     * @return Collection|Atostogos[]
     */
    public function getAtostogos(): Collection
    {
        return $this->atostogos;
    }

    public function addAtostogo(Atostogos $atostogo): self
    {
        if (!$this->atostogos->contains($atostogo)) {
            $this->atostogos[] = $atostogo;
            $atostogo->setTipas($this);
        }

        return $this;
    }

    public function removeAtostogo(Atostogos $atostogo): self
    {
        if ($this->atostogos->contains($atostogo)) {
            $this->atostogos->removeElement($atostogo);
            // set the owning side to null (unless already changed)
            if ($atostogo->getTipas() === $this) {
                $atostogo->setTipas(null);
            }
        }

        return $this;
    }
}
