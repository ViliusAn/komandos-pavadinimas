<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UzsakymasRepository")
 */
class Uzsakymas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $sukurimo_data;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $pristatymo_data;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $patvirtinimo_data;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="uzsakymas")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UzsakymoBusena", inversedBy="uzsakymas")
     */
    private $busena;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Siuntinys", cascade={"persist", "remove"})
     */
    private $siuntinys;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Atsiliepimas", mappedBy="uzsakymas", cascade={"persist", "remove"})
     */
    private $atsiliepimas;

    public function __construct()
    {
        $this->sukurimo_data = new DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSukurimoData(): ?\DateTimeInterface
    {
        return $this->sukurimo_data;
    }

    public function setSukurimoData(\DateTimeInterface $sukurimo_data): self
    {
        $this->sukurimo_data = $sukurimo_data;

        return $this;
    }

    public function getPristatymoData(): ?\DateTimeInterface
    {
        return $this->pristatymo_data;
    }

    public function setPristatymoData(\DateTimeInterface $pristatymo_data): self
    {
        $this->pristatymo_data = $pristatymo_data;

        return $this;
    }

    public function getPatvirtinimoData(): ?\DateTimeInterface
    {
        return $this->patvirtinimo_data;
    }

    public function setPatvirtinimoData(\DateTimeInterface $patvirtinimo_data): self
    {
        $this->patvirtinimo_data = $patvirtinimo_data;

        return $this;
    }

    public function getAdresas(): ?string
    {
        return $this->adresas;
    }

    public function setAdresas(string $adresas): self
    {
        $this->adresas = $adresas;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBusena(): ?UzsakymoBusena
    {
        return $this->busena;
    }

    public function setBusena(?UzsakymoBusena $busena): self
    {
        $this->busena = $busena;

        return $this;
    }

    public function getSiuntinys(): ?Siuntinys
    {
        return $this->siuntinys;
    }

    public function setSiuntinys(?Siuntinys $siuntinys): self
    {
        $this->siuntinys = $siuntinys;

        return $this;
    }

    public function getAtsiliepimas(): ?Atsiliepimas
    {
        return $this->atsiliepimas;
    }

    public function setAtsiliepimas(?Atsiliepimas $atsiliepimas): self
    {
        $this->atsiliepimas = $atsiliepimas;

        // set (or unset) the owning side of the relation if necessary
        $newUzsakymas = null === $atsiliepimas ? null : $this;
        if ($atsiliepimas->getUzsakymas() !== $newUzsakymas) {
            $atsiliepimas->setUzsakymas($newUzsakymas);
        }

        return $this;
    }
}
