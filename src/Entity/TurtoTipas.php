<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TurtoTipasRepository")
 */
class TurtoTipas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipas;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipas(): ?string
    {
        return $this->tipas;
    }

    public function setTipas(string $tipas): self
    {
        $this->tipas = $tipas;

        return $this;
    }

    public function __toString(): string {
        return $this->getTipas();
    }
}
