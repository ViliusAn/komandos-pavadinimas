<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DarbuotojasRepository")
 */
class Darbuotojas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telefono_nr;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $banko_saskaitos_nr;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sandelis", inversedBy="darbuotojai")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sandelis_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Zymejimasis", mappedBy="darbuotojas")
     */
    private $pasizymejimas;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $naudotojas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DarbuotojoPareigos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pareigos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Atostogos", mappedBy="darbuotojas")
     */
    private $atostogos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Dokumentas", mappedBy="darbuotojas")
     */
    private $dokumentai;

    public function __toString(): string {
        $string = $this->getId();
        return $string;
    }

    public function __construct()
    {
        $this->pasizymejimas = new ArrayCollection();
        $this->atostogos = new ArrayCollection();
        $this->dokumentai = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTelefonoNr(): ?string
    {
        return $this->telefono_nr;
    }

    public function setTelefonoNr(string $telefono_nr): self
    {
        $this->telefono_nr = $telefono_nr;

        return $this;
    }

    public function getBankoSaskaitosNr(): ?string
    {
        return $this->banko_saskaitos_nr;
    }

    public function setBankoSaskaitosNr(string $banko_saskaitos_nr): self
    {
        $this->banko_saskaitos_nr = $banko_saskaitos_nr;

        return $this;
    }

    public function getSandelisId(): ?Sandelis
    {
        return $this->sandelis_id;
    }

    public function setSandelisId(?Sandelis $sandelis_id): self
    {
        $this->sandelis_id = $sandelis_id;

        return $this;
    }

    /**
     * @return Collection|Zymejimasis[]
     */
    public function getPasizymejimas(): Collection
    {
        return $this->pasizymejimas;
    }

    public function addPasizymejima(Zymejimasis $pasizymejima): self
    {
        if (!$this->pasizymejimas->contains($pasizymejima)) {
            $this->pasizymejimas[] = $pasizymejima;
            $pasizymejima->setDarbuotojas($this);
        }

        return $this;
    }

    public function removePasizymejima(Zymejimasis $pasizymejima): self
    {
        if ($this->pasizymejimas->contains($pasizymejima)) {
            $this->pasizymejimas->removeElement($pasizymejima);
            // set the owning side to null (unless already changed)
            if ($pasizymejima->getDarbuotojas() === $this) {
                $pasizymejima->setDarbuotojas(null);
            }
        }

        return $this;
    }


    public function getNaudotojas(): ?User
    {
        return $this->naudotojas;
    }

    public function setNaudotojas(User $naudotojas): self
    {
        $this->naudotojas = $naudotojas;

        return $this;
    }

    public function getPareigos(): ?DarbuotojoPareigos
    {
        return $this->pareigos;
    }

    public function setPareigos(?DarbuotojoPareigos $pareigos): self
    {
        $this->pareigos = $pareigos;

        return $this;
    }

    /**
     * @return Collection|Atostogos[]
     */
    public function getAtostogos(): Collection
    {
        return $this->atostogos;
    }

    public function addAtostogo(Atostogos $atostogo): self
    {
        if (!$this->atostogos->contains($atostogo)) {
            $this->atostogos[] = $atostogo;
            $atostogo->setDarbuotojas($this);
        }

        return $this;
    }

    public function removeAtostogo(Atostogos $atostogo): self
    {
        if ($this->atostogos->contains($atostogo)) {
            $this->atostogos->removeElement($atostogo);
            // set the owning side to null (unless already changed)
            if ($atostogo->getDarbuotojas() === $this) {
                $atostogo->setDarbuotojas(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Dokumentas[]
     */
    public function getDokumentai(): Collection
    {
        return $this->dokumentai;
    }

    public function addDokumentai(Dokumentas $dokumentai): self
    {
        if (!$this->dokumentai->contains($dokumentai)) {
            $this->dokumentai[] = $dokumentai;
            $dokumentai->setDarbuotojas($this);
        }

        return $this;
    }

    public function removeDokumentai(Dokumentas $dokumentai): self
    {
        if ($this->dokumentai->contains($dokumentai)) {
            $this->dokumentai->removeElement($dokumentai);
            // set the owning side to null (unless already changed)
            if ($dokumentai->getDarbuotojas() === $this) {
                $dokumentai->setDarbuotojas(null);
            }
        }

        return $this;
    }
}
