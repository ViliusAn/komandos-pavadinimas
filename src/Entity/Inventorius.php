<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InventoriusRepository")
 */
class Inventorius
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $uzsakymo_data;

    /**
     * @ORM\Column(type="integer")
     */
    private $kiekis;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $uzsake;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sandelis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sandelis_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\InventoriausTipas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tipas;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $komentaras;

    /**
     * @ORM\Column(type="boolean")
     */
    private $yra_sandelyje;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUzsakymoData(): ?\DateTimeInterface
    {
        return $this->uzsakymo_data;
    }

    public function setUzsakymoData(\DateTimeInterface $uzsakymo_data): self
    {
        $this->uzsakymo_data = $uzsakymo_data;

        return $this;
    }

    public function getKiekis(): ?int
    {
        return $this->kiekis;
    }

    public function setKiekis(int $kiekis): self
    {
        $this->kiekis = $kiekis;

        return $this;
    }

    public function getUzsake(): ?User
    {
        return $this->uzsake;
    }

    public function setUzsake(?User $uzsake): self
    {
        $this->uzsake = $uzsake;

        return $this;
    }

    public function getSandelisId(): ?Sandelis
    {
        return $this->sandelis_id;
    }

    public function setSandelisId(?Sandelis $sandelis_id): self
    {
        $this->sandelis_id = $sandelis_id;

        return $this;
    }

    public function getTipas(): ?InventoriausTipas
    {
        return $this->tipas;
    }

    public function setTipas(?InventoriausTipas $tipas): self
    {
        $this->tipas = $tipas;

        return $this;
    }

    public function getKomentaras(): ?string
    {
        return $this->komentaras;
    }

    public function setKomentaras(?string $komentaras): self
    {
        $this->komentaras = $komentaras;

        return $this;
    }

    public function getYraSandelyje(): ?bool
    {
        return $this->yra_sandelyje;
    }

    public function setYraSandelyje(bool $yra_sandelyje): self
    {
        $this->yra_sandelyje = $yra_sandelyje;

        return $this;
    }

    public function __toString(): string {
        return $this->getId();
    }
}
