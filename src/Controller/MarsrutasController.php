<?php

namespace App\Controller;

use App\Entity\Marsrutas;
use App\Form\MarsrutasType;
use App\Repository\MarsrutasRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/marsrutas")
 */
class MarsrutasController extends AbstractController
{
    /**
     * @Route("/", name="marsrutas_index", methods={"GET"})
     */
    public function index(MarsrutasRepository $marsrutasRepository): Response
    {
        return $this->render('marsrutas/index.html.twig', [
            'marsrutas' => $marsrutasRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="marsrutas_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $marsruta = new Marsrutas();
        $form = $this->createForm(MarsrutasType::class, $marsruta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($marsruta);
            $entityManager->flush();

            return $this->redirectToRoute('marsrutas_index');
        }

        return $this->render('marsrutas/new.html.twig', [
            'marsruta' => $marsruta,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="marsrutas_show", methods={"GET"})
     */
    public function show(Marsrutas $marsruta): Response
    {
        return $this->render('marsrutas/show.html.twig', [
            'marsruta' => $marsruta,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="marsrutas_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Marsrutas $marsruta): Response
    {
        $form = $this->createForm(MarsrutasType::class, $marsruta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('marsrutas_index');
        }

        return $this->render('marsrutas/edit.html.twig', [
            'marsruta' => $marsruta,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="marsrutas_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Marsrutas $marsruta): Response
    {
        if ($this->isCsrfTokenValid('delete'.$marsruta->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($marsruta);
            $entityManager->flush();
        }

        return $this->redirectToRoute('marsrutas_index');
    }
}
