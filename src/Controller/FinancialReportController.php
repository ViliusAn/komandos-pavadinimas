<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Skola;
use App\Entity\Apmokejimas;
use App\Entity\Turtas;
use App\Repository\SkolaRepository;
use App\Repository\ApmokejimasRepository;
use App\Repository\TurtasRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
// use App\Entity\Apmokejimas;
// use App\Entity\FinansinisRodiklis;

class FinancialReportController extends AbstractController
{
    /**
     * @Route("/financial/report_form", name="financial_report_form")
     */
    public function index()
    {
        return $this->render('financial_report/index.html.twig', [
            'controller_name' => 'FinancialReportController',
        ]);
    }

	/**
	 * @Route("/financial/report", name="generate_financial_report", methods={"GET", "POST"})
	 */
	public function report(Request $request) : Response
	{
        $nuo = $request->get('report_from');
        $iki = $request->get('report_to');

        //get data
        $repository = $this->getDoctrine()->getRepository(Skola::class);
        $debts = $repository->getAllDebts($nuo, $iki);

        $repository = $this->getDoctrine()->getRepository(Apmokejimas::class);
        $payments = $repository->getAllPayments($nuo, $iki);

        $repository = $this->getDoctrine()->getRepository(Turtas::class);
        $equityFromTo = $repository->getAllEquityByPeriod($nuo, $iki);
        $allEquity = $repository->getAllEquity();
        //get data

        $totalDebt = $this->calculateTotalDebt($debts);
        $totalSales = $this->calculateSales($payments);
        $boughtEquity = $this->calculateAcquiredEquity($equityFromTo);
        $totalEquity = $this->calculateTotalEquity($allEquity);
        $profit = $totalSales - $boughtEquity;
        
        // die("mirstam");
        return $this->render('financial_report/report.html.twig', [
            'debt' => $totalDebt,
            'sales' => $totalSales,
            'bought_equity' => $boughtEquity,
            'all_equity' => $totalEquity,
            'profit' => $profit,
            'from' => $nuo,
            'to' => $iki,
            'ataskaita' => false
            ]);
    }

    /**
	 * @Route("/financial/report/download", name="download_financial_report", methods={"GET", "POST"})
	 */
    public function downloadReport(Request $request) : Response
    {
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        
        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        $date = date("Y-m-d");

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('financial_report/report.html.twig', [
            'debt' => $request->get('debt'),
            'sales' => $request->get('sales'),
            'bought_equity' => $request->get('bought_equity'),
            'all_equity' => $request->get('all_equity'),
            'profit' => $request->get('profit'),
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'ataskaita' => true,
            'data' => $date
        ]);
        
        // Load HTML to Dompdf
        $dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        $pavadinimas = "ataskaita".$date.".pdf";

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($pavadinimas, [
            "Attachment" => true
        ]);
    }

    private function calculateTotalDebt($debts)
    {
        $totalDebt = 0;
        foreach($debts as $debt) {
            $totalDebt += $debt['dydis'] - $debt['grazinta_suma'];
        }
        return $totalDebt;
    }

    private function calculateSales($payments)
    {
        $totalSales = 0;
        foreach($payments as $payment) {
            $totalSales += $payment['kiekis'];
        }

        return $totalSales;
    }

    private function calculateAcquiredEquity($Equity)
    {
        $totalEquity = 0;
        foreach($Equity as $e) {
            $totalEquity += $e['verte'];
        }

        return $totalEquity;
    }

    private function calculateTotalEquity($Equity)
    {
        $totalEquity = 0;
        foreach($Equity as $e) {
            $totalEquity += $e['verte'];
        }

        return $totalEquity;
    }
}
