<?php

namespace App\Controller;

use App\Entity\Sandelis;
use App\Form\SandelisType;
use App\Repository\SandelisRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sandelis")
 */
class SandelisController extends AbstractController
{
    /**
     * @Route("/", name="sandelis_index", methods={"GET"})
     */
    public function index(SandelisRepository $sandelisRepository): Response
    {
        return $this->render('sandelis/index.html.twig', [
            'sandelis' => $sandelisRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sandelis_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sandeli = new Sandelis();
        $form = $this->createForm(SandelisType::class, $sandeli);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sandeli);
            $entityManager->flush();

            return $this->redirectToRoute('sandelis_index');
        }

        return $this->render('sandelis/new.html.twig', [
            'sandeli' => $sandeli,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sandelis_show", methods={"GET"})
     */
    public function show(Sandelis $sandeli): Response
    {
        return $this->render('sandelis/show.html.twig', [
            'sandeli' => $sandeli,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sandelis_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sandelis $sandeli): Response
    {
        $form = $this->createForm(SandelisType::class, $sandeli);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sandelis_index');
        }

        return $this->render('sandelis/edit.html.twig', [
            'sandeli' => $sandeli,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sandelis_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Sandelis $sandeli): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sandeli->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sandeli);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sandelis_index');
    }
}
