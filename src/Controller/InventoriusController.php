<?php

namespace App\Controller;

use App\Entity\Inventorius;
use App\Form\InventoriusType;
use App\Repository\InventoriusRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/inventorius")
 */
class InventoriusController extends AbstractController
{
    /**
     * @Route("/", name="inventorius_index", methods={"GET"})
     */
    public function index(InventoriusRepository $inventoriusRepository): Response
    {
        return $this->render('inventorius/index.html.twig', [
            'inventoriuses' => $inventoriusRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="inventorius_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $inventorius = new Inventorius();
        $form = $this->createForm(InventoriusType::class, $inventorius);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $time = new \DateTime();
            $time->format('H:i:s \O\n Y-m-d');
            $inventorius->setUzsakymoData($time);
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $inventorius->setUzsake($user);
            $entityManager->persist($inventorius);
            $entityManager->flush();

            return $this->redirectToRoute('inventorius_index');
        }

        return $this->render('inventorius/new.html.twig', [
            'inventorius' => $inventorius,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="inventorius_show", methods={"GET"})
     */
    public function show(Inventorius $inventorius): Response
    {
        return $this->render('inventorius/show.html.twig', [
            'inventorius' => $inventorius,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="inventorius_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Inventorius $inventorius): Response
    {
        $form = $this->createForm(InventoriusType::class, $inventorius);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('inventorius_index');
        }

        return $this->render('inventorius/edit.html.twig', [
            'inventorius' => $inventorius,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="inventorius_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Inventorius $inventorius): Response
    {
        if ($this->isCsrfTokenValid('delete'.$inventorius->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($inventorius);
            $entityManager->flush();
        }

        return $this->redirectToRoute('inventorius_index');
    }

    /**
	 * @Route("/inventory/requests", name="inventory_requests")
	 */
	public function requests()
	{
        $requests = $this->getDoctrine()->getRepository(Inventorius::class)->findBy(
            ['yra_sandelyje'=>0]
        );
        return $this->render('inventory/requests.html.twig', ['requests' => $requests]);
    }

    /**
     * @Route("/{id}/confirm", name="inventorius_confirm", methods={"GET", "POST"})
     */
    public function inventoriusConfirm(Request $request, Inventorius $inventorius): Response
    {
        $id = $request->get('id');
        $entityManager = $this->getDoctrine()->getManager();
        $item = $entityManager->getRepository(Inventorius::class)->find($id);
        if (!$item) {
            throw $this->createNotFoundException(
                'No request found for id '.$id
            );
        }
        $item->setYraSandelyje(true);
        $entityManager->flush();
        return $this->redirectToRoute('inventory_requests');
    }
}