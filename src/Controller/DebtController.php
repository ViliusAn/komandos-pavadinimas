<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DebtController extends AbstractController
{
    /**
     * @Route("/debt", name="debt")
     */
    public function index()
    {
        return $this->render('debt/index.html.twig', [
            'controller_name' => 'DebtController',
        ]);
    }

	/**
	 * @Route("/debt/report", name="debt_report")
	 */
	public function report()
	{
		return $this->render('debt/report.html.twig', [

		]);
	}

	/**
	 * @Route("/debt/register", name="debt_register")
	 */
	public function register()
	{
		return $this->render('debt/register.html.twig', []);
	}
}
