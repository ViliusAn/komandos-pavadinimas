<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EquityController extends AbstractController
{
    /**
     * @Route("/equity", name="equity")
     */
    public function index()
    {
        return $this->render('equity/index.html.twig', [
            'controller_name' => 'EquityController',
        ]);
    }
}
