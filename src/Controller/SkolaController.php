<?php

namespace App\Controller;

use App\Entity\Skola;
use App\Form\SkolaType;
use App\Repository\SkolaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/skola")
 */
class SkolaController extends AbstractController
{
    /**
     * @Route("/", name="skola_index", methods={"GET"})
     */
    public function index(SkolaRepository $skolaRepository): Response
    {
        return $this->render('skola/index.html.twig', [
            'skolas' => $skolaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="skola_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $skola = new Skola();
        $form = $this->createForm(SkolaType::class, $skola);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($skola);
            $entityManager->flush();

            return $this->redirectToRoute('skola_index');
        }

        return $this->render('skola/new.html.twig', [
            'skola' => $skola,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="skola_show", methods={"GET"})
     */
    public function show(Skola $skola): Response
    {
        return $this->render('skola/show.html.twig', [
            'skola' => $skola,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="skola_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Skola $skola): Response
    {
        $form = $this->createForm(SkolaType::class, $skola);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('skola_index');
        }

        return $this->render('skola/edit.html.twig', [
            'skola' => $skola,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="skola_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Skola $skola): Response
    {
        if ($this->isCsrfTokenValid('delete'.$skola->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($skola);
            $entityManager->flush();
        }

        return $this->redirectToRoute('skola_index');
    }
}
