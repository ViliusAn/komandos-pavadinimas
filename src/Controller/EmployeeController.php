<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\DarbuotojasType;
use App\Entity\Darbuotojas;
use App\Form\DokumentasType;
use App\Entity\Dokumentas;
use App\Entity\DokumentoTipas;
use App\Form\VirsvalandziaiType;
use App\Entity\Virsvalandziai;
use App\Repository\VirsvalandziaiRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
* @Route("/employees")
*/
class EmployeeController extends AbstractController
{
    /**
     * @Route("/create", name="employee_create")
     * Method({"GET", "POST"})
     */
    public function employee_create(Request $request)
    {
        $darbuotojas = new Darbuotojas();

        $form = $this->createForm(DarbuotojasType::class, $darbuotojas, [
            'action' => $this->generateUrl('employee_create')
        ]);


        $form -> handleRequest($request);
        if($form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();
            $darbuotojas = $form -> getData();

            $naudotojas= $this->get('security.token_storage')->getToken()->getUser();
            $darbuotojas -> setNaudotojas($naudotojas);
            
            $em->persist($darbuotojas);
            $em->flush();
            $this->addFlash('success', 'Darbuotojo informacija sukurta sėkmingai.');
        }
        return $this->render('employee/create.html.twig', [
            'controller_name' => 'Darbuotojo sukūrimo forma',
            'employee_create' => $form->createView()
        ]);
        
    }

    /**
     * @Route("/edit", name="employee_edit")
     * Method({"GET", "POST"})
     */
    public function employee_edit(Request $request)
    {
        $darbuotojas = new Darbuotojas();
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $naudotojas= $this->get('security.token_storage')->getToken()->getUser();
       
        $naudotojo_id = $propertyAccessor->getValue($naudotojas, 'id');
        $darbuotojas = $this->getDoctrine()->getRepository(Darbuotojas::class)->findBy(array('naudotojas' => $naudotojo_id),array('naudotojas' => 'ASC'),1 ,0)[0];
        $darbuotojo_id = $propertyAccessor->getValue($darbuotojas, 'id');
        //dump($darbuotojo_id); die;
        $form = $this->createForm(DarbuotojasType::class, $darbuotojas, [
            'action' => $this->generateUrl('employee_edit')
        ]);

        $form -> handleRequest($request);
        if($form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('success', 'Darbuotojo informacija atnaujinta sėkmingai.');
        }
        return $this->render('employee/edit.html.twig', [
            'controller_name' => 'Darbuotojo informacijos redagavimas',
            'employee_edit' => $form->createView()
        ]);
    }

    /**
     * @Route("/work/effectiveness", name="work_effectiveness")
     */
    public function work_effectiveness()
    {
        return $this->render('employee/work_effectiveness.html.twig', [
            'controller_name' => 'Darbuotojo efektyvumo skaičiavimo forma',
        ]);
    }

    /**
     * @Route("/work/effectiveness/display", name="work_effectiveness_display")
     */
    public function work_effectiveness_display()
    {
        return $this->render('employee/work_effectiveness_display.html.twig', [
            'controller_name' => 'Darbuotojo efektyvumo rezultatai',
        ]);
    }
    
    /**
     * @Route("/misdemeanour/create", name="misdemeanour_create")
     * Method({"GET", "POST"})
     */
    public function misdemeanour_create(Request $request)
    {
        $nusizengimas = new Dokumentas();

        $form = $this->createForm(DokumentasType::class, $nusizengimas, [
            'action' => $this->generateUrl('misdemeanour_create')
        ]);
        
        $form -> handleRequest($request);
        if($form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();
            $nusizengimas = $form -> getData();
            $time = new \DateTime();
            $time->format('H:i:s \O\n Y-m-d');
            $dokumento_tipas = $this->getDoctrine()->getRepository(DokumentoTipas::class)->findBy(array('id' => '5'),array('id' => 'ASC'),1 ,0)[0];
            $tipas = 'nusizengimas';
            $nusizengimas -> setSudarymoData($time);
            $nusizengimas -> setTipas($dokumento_tipas);
            
            $em->persist($nusizengimas);
            $em->flush();
            $this->addFlash('success', 'Nusižengimas užregistruotas sėkmingai.');
        }

        return $this->render('employee/misdemeanour_create.html.twig', [
            'controller_name' => 'Nusižengimų registravimo forma',
            'misdemeanour_create' => $form->createView()
        ]);
    }

    /**
     * @Route("/overtime/create", name="overtime_create")
     * Method({"GET", "POST"})
     */
    public function overtime_create(Request $request)
    {
        $virsvalandziai = new Virsvalandziai();

        $form = $this->createForm(VirsvalandziaiType::class, $virsvalandziai, [
            'action' => $this->generateUrl('overtime_create')
        ]);
        $form -> handleRequest($request);

        if($form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($virsvalandziai);
            $em->flush();
            $this->addFlash('success', 'Viršvalandžiai uzregistruoti sėkmingai.');
        }

        return $this->render('employee/overtime_create.html.twig', [
            'controller_name' => 'Viršvalandžių registravimo forma',
            'overtime_create' => $form->createView()
        ]);
    }

    /**
     * @Route("/overtime/index", name="overtime_index")
     */
    public function overtime_index(VirsvalandziaiRepository $virsvalandziaiRepository)
    {

        return $this->render('employee/overtime_index.html.twig', [
            'controller_name' => 'Viršvalandžių atvaizdavimas',
            'virsvalandziai' => $virsvalandziaiRepository->findAll()
        ]);
    }

    /**
     * @Route("/best", name="best_worker_find")
     */
    public function best_worker_find()
    {
        return $this->render('employee/best_worker_find.html.twig', [
            'controller_name' => 'Geriausio mėnesio darbuotojo radimo forma',
        ]);
    }
        /**
     * @Route("/best/display", name="best_worker_display")
     */
    public function best_worker_display()
    {
        return $this->render('employee/best_worker_display.html.twig', [
            'controller_name' => 'Geriausio mėnesio darbuotojo rezultai',
        ]);
    }
}
