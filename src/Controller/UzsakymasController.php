<?php

namespace App\Controller;

use App\Entity\Atsiliepimas;
use App\Entity\Uzsakymas;
use App\Entity\UzsakymoBusena;
use App\Form\AtsiliepimasType;
use App\Form\UzsakymasType;
use App\Repository\UzsakymasRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/uzsakymas")
 */
class UzsakymasController extends AbstractController
{
    /**
     * @Route("/", name="uzsakymas_index", methods={"GET"})
     */
    public function index(UzsakymasRepository $uzsakymasRepository): Response
    {
        $user = $this->getUser();
        $uzsakymai = null;
        if ($user->getUserType()->getName() == 'Admin') {
            $uzsakymai = $uzsakymasRepository->findAll();
        } else {
            $uzsakymai = $uzsakymasRepository->findByUser($user->getId());
        }
        return $this->render('uzsakymas/index.html.twig', [
            'uzsakymas' => $uzsakymai,
            'controller_name' => 'Užsakymų sąrašas',
        ]);
    }

    /**
     * @Route("/new", name="uzsakymas_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $uzsakymas = new Uzsakymas();
        $user = $this->getUser();
        $form = $this->createForm(UzsakymasType::class, $uzsakymas);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tipas = $this->getDoctrine()->getRepository(UzsakymoBusena::class)->findOneBySomeField('Vykdomas');
            $uzsakymas->setBusena($tipas);
            $uzsakymas->setUser($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($uzsakymas);
            $entityManager->flush();

            return $this->redirectToRoute('uzsakymas_index');
        }

        return $this->render('uzsakymas/new.html.twig', [
            'uzsakyma' => $uzsakymas,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="uzsakymas_show", methods={"GET", "POST"})
     */
    public function show(Request $request, Uzsakymas $uzsakyma): Response
    {
        $user = $this->getUser();
        if ($user->getUserType()->getName() != 'Admin' && $uzsakyma->getUser()->getId() != $user->getId()) {
            return $this->redirectToRoute('uzsakymas_index');
        }
        $atsiliepima = null;
        $formView = null;
        if (!$uzsakyma->getAtsiliepimas()) {
            $atsiliepima = new Atsiliepimas();
            $form = $this->createForm(AtsiliepimasType::class, $atsiliepima);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $atsiliepima->setUser($user);
                $atsiliepima->setUzsakymas($uzsakyma);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($atsiliepima);
                $entityManager->flush();

                $this->addFlash('success', 'Atsiliepimas sėkmingai parašytas');

                return $this->redirectToRoute('uzsakymas_show', array('id' => $uzsakyma->getId()));
            }

            $formView = $form->createView();
        }
        return $this->render('uzsakymas/show.html.twig', [
            'uzsakyma' => $uzsakyma,
            'atsiliepima' => $atsiliepima,
            'form' => $formView,
            'edit' => false,
        ]);
    }

    /**
     * @Route("/show/{id}/edit", name="uzsakymas_show_edit", methods={"GET", "POST"})
     */
    public function show_edit(Request $request, Uzsakymas $uzsakyma): Response
    {
        if(!$uzsakyma->getAtsiliepimas()) {
            return $this->redirectToRoute('uzsakymas_show', array('id' => $uzsakyma->getId()));
        }
        $user = $this->getUser();
        if ($user->getUserType()->getName() != 'Admin' && $uzsakyma->getUser()->getId() != $user->getId()) {
            return $this->redirectToRoute('uzsakymas_index');
        }
        $atsiliepima = null;
        $formView = null;
        if ($uzsakyma->getAtsiliepimas()) {
            $form = $this->createForm(AtsiliepimasType::class, $uzsakyma->getAtsiliepimas());
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->flush();

                $this->addFlash('success', 'Atsiliepimas sėkmingai atnaujintas');

                return $this->redirectToRoute('uzsakymas_show', array('id' => $uzsakyma->getId()));
            }

            $formView = $form->createView();
        }
        return $this->render('uzsakymas/show.html.twig', [
            'uzsakyma' => $uzsakyma,
            'atsiliepima' => $atsiliepima,
            'form' => $formView,
            'edit' => true,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="uzsakymas_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Uzsakymas $uzsakymas): Response
    {
        $user = $this->getUser();
        if ($user->getUserType()->getName() != 'Admin' && $uzsakymas->getUser()->getId() != $user->getId()) {
            return $this->redirectToRoute('uzsakymas_index');
        }
        $showUserForm = null;
        if ($user->getUserType()->getName() == 'Admin') {
            $showUserForm = true;
        }
        $form = $this->createForm(UzsakymasType::class, $uzsakymas, array('showUser' => $showUserForm));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($uzsakymas->getBusena()->getPavadinimas() === "Patvirtintas") {
                $this->addFlash('error', 'Užsakymo redaguoti negalite, nes jis jau yra patvirtintas');
            } else {
                /* $busena = $form->get('busena')->getData();
                if($busena == 'Patvirtintas') {
                    $uzsakymas->setPatvirtinimoData(new DateTime());
                } */
                $this->addFlash('success', 'Užsakymas sėkmingai atnaujintas');
                $this->getDoctrine()->getManager()->flush();
            }

            return $this->redirectToRoute('uzsakymas_index');
        }

        return $this->render('uzsakymas/edit.html.twig', [
            'uzsakyma' => $uzsakymas,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="uzsakymas_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="Neturite tokių teisių")
     */
    public function delete(Request $request, Uzsakymas $uzsakyma): Response
    {
        if ($this->isCsrfTokenValid('delete' . $uzsakyma->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($uzsakyma);
            $entityManager->flush();
        }

        return $this->redirectToRoute('uzsakymas_index');
    }

    /**
     * @Route("/patvirtinti/{id}", name="uzsakymas_approve", methods={"POST"})
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="Neturite tokių teisių")
     */
    public function approve(Request $request, Uzsakymas $uzsakyma): Response
    {
        if ($this->isCsrfTokenValid('approve' . $uzsakyma->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $tipas = $this->getDoctrine()->getRepository(UzsakymoBusena::class)->findOneBySomeField('Patvirtintas');
            $uzsakyma->setBusena($tipas);
            $uzsakyma->setPatvirtinimoData(new DateTime());
            $entityManager->flush();
        }

        return $this->redirectToRoute('uzsakymas_index');
    }

    /**
     * @Route("/atmesti/{id}", name="uzsakymas_deny", methods={"POST"})
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="Neturite tokių teisių")
     */
    public function deny(Request $request, Uzsakymas $uzsakyma): Response
    {
        if ($this->isCsrfTokenValid('deny' . $uzsakyma->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $tipas = $this->getDoctrine()->getRepository(UzsakymoBusena::class)->findOneBySomeField('Atmestas');
            $uzsakyma->setBusena($tipas);
            $entityManager->flush();
        }

        return $this->redirectToRoute('uzsakymas_index');
    }

    /**
     * @Route("/nepatvirtinti", name="uzsakymas_waiting", methods={"GET"})
     */
    public function waiting_list(UzsakymasRepository $uzsakymasRepository): Response
    {
        $tipas = $this->getDoctrine()->getRepository(UzsakymoBusena::class)->findOneBySomeField('Vykdomas');
        $user = $this->getUser();
        $uzsakymai = null;
        if ($user->getUserType()->getName() == 'Admin') {
            $uzsakymai = $uzsakymasRepository->findByBusena($tipas->getId());
        } else {
            $uzsakymai = $uzsakymasRepository->findByUserAndBusena($user->getId(), $tipas->getId());
        }
        return $this->render('uzsakymas/index.html.twig', [
            'uzsakymas' => $uzsakymai,
            'controller_name' => 'Nepatvirtinti užsakymai',
        ]);
    }
}
