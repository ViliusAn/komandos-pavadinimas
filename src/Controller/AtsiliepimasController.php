<?php

namespace App\Controller;

use App\Entity\Atsiliepimas;
use App\Form\AtsiliepimasType;
use App\Repository\AtsiliepimasRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/atsiliepimas")
 */
class AtsiliepimasController extends AbstractController
{
    /**
     * @Route("/", name="atsiliepimas_index", methods={"GET"})
     */
    public function index(AtsiliepimasRepository $atsiliepimasRepository): Response
    {
        return $this->render('atsiliepimas/index.html.twig', [
            'atsiliepimas' => $atsiliepimasRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="atsiliepimas_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $atsiliepima = new Atsiliepimas();
        $form = $this->createForm(AtsiliepimasType::class, $atsiliepima);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($atsiliepima);
            $entityManager->flush();

            return $this->redirectToRoute('atsiliepimas_index');
        }

        return $this->render('atsiliepimas/new.html.twig', [
            'atsiliepima' => $atsiliepima,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="atsiliepimas_show", methods={"GET"})
     */
    public function show(Atsiliepimas $atsiliepima): Response
    {
        return $this->render('atsiliepimas/show.html.twig', [
            'atsiliepima' => $atsiliepima,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="atsiliepimas_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Atsiliepimas $atsiliepima): Response
    {
        $form = $this->createForm(AtsiliepimasType::class, $atsiliepima);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('atsiliepimas_index');
        }

        return $this->render('atsiliepimas/edit.html.twig', [
            'atsiliepima' => $atsiliepima,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="atsiliepimas_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Atsiliepimas $atsiliepima): Response
    {
        $id = $atsiliepima->getUzsakymas()->getId();
        if ($this->isCsrfTokenValid('delete'.$atsiliepima->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($atsiliepima);
            $entityManager->flush();
        }

        return $this->redirectToRoute('uzsakymas_show', array('id' => $id));
    }
}
