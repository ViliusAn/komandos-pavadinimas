<?php

namespace App\Controller;

use App\Entity\Turtas;
use App\Form\TurtasType;
use App\Repository\TurtasRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\TurtoTipas;

/**
 * @Route("/turtas")
 */
class TurtasController extends AbstractController
{    
    /**
    * @Route("/transportas", name="turtas_index_transportas", methods={"GET"})
    */
    public function indexTransport(TurtasRepository $turtasRepository): Response
    {
        return $this->render('turtas/indextransport.html.twig', [
            'turtas' => $turtasRepository->findBy(
                [
                    'tipas' => 1,
                ]
            ),
        ]);
    }

    /**
     * @Route("/transportas/new", name="turtas_transportas_new", methods={"GET", "POST"})
     */
    public function newTransport(Request $request): Response
    {
        $turta = new Turtas();
        $form = $this->createForm(TurtasType::class, $turta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();    
            $repository = $this->getDoctrine()->getRepository(TurtoTipas::class);
            $tipas = $repository->find(1);
            $turta->setTipas($tipas);
            $entityManager->persist($turta);
            $entityManager->flush();

            return $this->redirectToRoute('turtas_index_transportas');
        }

        return $this->render('turtas/newtransport.html.twig', [
            'turta' => $turta,
            'form' => $form->createView(),
        ]);
    }



    /**
     * @Route("/turtas", name="turtas_index", methods={"GET"})
     */
    public function index(TurtasRepository $turtasRepository): Response
    {
        return $this->render('turtas/index.html.twig', [
            'turtas' => $turtasRepository->findBy(
                [
                    'tipas' => 2,
                ]
            ),
        ]);
    }

    /**
     * @Route("/turtas/new", name="turtas_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $turta = new Turtas();
        // $form = $this->createForm(TurtasType::class, $turta, array('ar_turtas' => true));
        $form = $this->createForm(TurtasType::class, $turta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $repository = $this->getDoctrine()->getRepository(TurtoTipas::class);
            $tipas = $repository->find(2);
            $turta->setTipas($tipas);
            $entityManager->persist($turta);
            $entityManager->flush();

            return $this->redirectToRoute('turtas_index');
        }

        return $this->render('turtas/new.html.twig', [
            'turta' => $turta,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="turtas_show", methods={"GET"})
     */
    public function show(Turtas $turta): Response
    {
        return $this->render('turtas/show.html.twig', [
            'turta' => $turta,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="turtas_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Turtas $turta): Response
    {
        $form = $this->createForm(TurtasType::class, $turta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('turtas_index');
        }

        return $this->render('turtas/edit.html.twig', [
            'turta' => $turta,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="turtas_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Turtas $turta): Response
    {
        if ($this->isCsrfTokenValid('delete'.$turta->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($turta);
            $entityManager->flush();
        }

        return $this->redirectToRoute('turtas_index');
    }
}
