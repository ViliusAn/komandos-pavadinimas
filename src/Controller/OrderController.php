<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/orders")
*/
class OrderController extends AbstractController
{
    /**
     * @Route("/", name="orders_pending")
     */
    public function orders_pending_list()
    {
        return $this->render('order/pending_list.html.twig', [
            'controller_name' => 'Nepatvirtinti užsakymai',
        ]);
    }

    /**
     * @Route("/create", name="order_create")
     */
    public function order_create()
    {
        return $this->render('order/create.html.twig', [
            'controller_name' => 'Užsakymo sukūrimas',
        ]);
    }

    /**
     * @Route("/edit", name="orders_edit")
     */
    public function orders_edit()
    {
        return $this->render('order/edit.html.twig', [
            'controller_name' => 'Užsakymo redagavimas',
        ]);
    }

    /**
     * @Route("/reviews/create", name="reviews_create")
     */
    public function reviews_create()
    {
        return $this->render('order/reviews_create.html.twig', [
            'controller_name' => 'Atsiliepimo kūrimas',
        ]);
    }

    /**
     * @Route("/reviews/edit", name="reviews_edit")
     */
    public function reviews_edit()
    {
        return $this->render('order/reviews_edit.html.twig', [
            'controller_name' => 'Atsiliepimų redagavimas',
        ]);
    }
}
