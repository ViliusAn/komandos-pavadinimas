<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191223113417 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE atsiliepimas (id INT AUTO_INCREMENT NOT NULL, uzsakymas_id INT DEFAULT NULL, user_id INT DEFAULT NULL, tekstas LONGTEXT NOT NULL, data DATETIME NOT NULL, antraste LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_E02D96638914E9BC (uzsakymas_id), INDEX IDX_E02D9663A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE atsiliepimas ADD CONSTRAINT FK_E02D96638914E9BC FOREIGN KEY (uzsakymas_id) REFERENCES uzsakymas (id)');
        $this->addSql('ALTER TABLE atsiliepimas ADD CONSTRAINT FK_E02D9663A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE uzsakymas CHANGE user_id user_id INT DEFAULT NULL, CHANGE busena_id busena_id INT DEFAULT NULL, CHANGE siuntinys_id siuntinys_id INT DEFAULT NULL, CHANGE pristatymo_data pristatymo_data DATETIME DEFAULT NULL, CHANGE patvirtinimo_data patvirtinimo_data DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE atsiliepimas');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE uzsakymas CHANGE user_id user_id INT DEFAULT NULL, CHANGE busena_id busena_id INT DEFAULT NULL, CHANGE siuntinys_id siuntinys_id INT DEFAULT NULL, CHANGE pristatymo_data pristatymo_data DATETIME DEFAULT \'NULL\', CHANGE patvirtinimo_data patvirtinimo_data DATETIME DEFAULT \'NULL\'');
    }
}
