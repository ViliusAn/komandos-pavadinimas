<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191221224915 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE zymejimasis (id INT AUTO_INCREMENT NOT NULL, data DATE NOT NULL, pasizymejimo_laikas TIME NOT NULL, atsizymejimo_laikas TIME NOT NULL, darbo_kiekis INT NOT NULL, darbo_tipas INT NOT NULL, virsvalandziai INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE darbuotojas_zymejimasis (darbuotojas_id INT NOT NULL, zymejimasis_id INT NOT NULL, INDEX IDX_694B5C717C8F88E2 (darbuotojas_id), INDEX IDX_694B5C71EAC81997 (zymejimasis_id), PRIMARY KEY(darbuotojas_id, zymejimasis_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE apmokejimas (id INT AUTO_INCREMENT NOT NULL, kliento_id_id INT NOT NULL, kiekis INT NOT NULL, data DATETIME NOT NULL, INDEX IDX_19B2DAE852BA4F47 (kliento_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE darbuotojas_zymejimasis ADD CONSTRAINT FK_694B5C717C8F88E2 FOREIGN KEY (darbuotojas_id) REFERENCES darbuotojas (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE darbuotojas_zymejimasis ADD CONSTRAINT FK_694B5C71EAC81997 FOREIGN KEY (zymejimasis_id) REFERENCES zymejimasis (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE apmokejimas ADD CONSTRAINT FK_19B2DAE852BA4F47 FOREIGN KEY (kliento_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE darbuotojas ADD vardas VARCHAR(255) NOT NULL, ADD pavarde VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE sandelis CHANGE adresas adresas VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE marsrutas ADD isvykimas DATETIME NOT NULL');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE darbuotojas_zymejimasis DROP FOREIGN KEY FK_694B5C71EAC81997');
        $this->addSql('DROP TABLE zymejimasis');
        $this->addSql('DROP TABLE darbuotojas_zymejimasis');
        $this->addSql('DROP TABLE apmokejimas');
        $this->addSql('ALTER TABLE darbuotojas DROP vardas, DROP pavarde');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE marsrutas DROP isvykimas');
        $this->addSql('ALTER TABLE sandelis CHANGE adresas adresas INT NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
