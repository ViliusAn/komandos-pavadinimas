<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191221230923 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE finansinio_rodiklio_tipas (id INT AUTO_INCREMENT NOT NULL, tipas VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE finansinis_rodiklis (id INT AUTO_INCREMENT NOT NULL, tipas_id INT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, data DATETIME NOT NULL, rodiklio_reiksme DOUBLE PRECISION NOT NULL, INDEX IDX_3827C7E51CEC0959 (tipas_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE finansinis_rodiklis ADD CONSTRAINT FK_3827C7E51CEC0959 FOREIGN KEY (tipas_id) REFERENCES finansinio_rodiklio_tipas (id)');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE zymejimasis CHANGE virsvalandziai virsvalandziai INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE finansinis_rodiklis DROP FOREIGN KEY FK_3827C7E51CEC0959');
        $this->addSql('DROP TABLE finansinio_rodiklio_tipas');
        $this->addSql('DROP TABLE finansinis_rodiklis');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE zymejimasis CHANGE virsvalandziai virsvalandziai INT DEFAULT NULL');
    }
}
