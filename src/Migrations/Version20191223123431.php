<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191223123431 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE atsiliepimas CHANGE uzsakymas_id uzsakymas_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE uzsakymas CHANGE user_id user_id INT DEFAULT NULL, CHANGE busena_id busena_id INT DEFAULT NULL, CHANGE siuntinys_id siuntinys_id INT DEFAULT NULL, CHANGE pristatymo_data pristatymo_data DATETIME DEFAULT NULL, CHANGE patvirtinimo_data patvirtinimo_data DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE atsiliepimas CHANGE uzsakymas_id uzsakymas_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE uzsakymas CHANGE user_id user_id INT DEFAULT NULL, CHANGE busena_id busena_id INT DEFAULT NULL, CHANGE siuntinys_id siuntinys_id INT DEFAULT NULL, CHANGE pristatymo_data pristatymo_data DATETIME DEFAULT \'NULL\', CHANGE patvirtinimo_data patvirtinimo_data DATETIME DEFAULT \'NULL\'');
    }
}
