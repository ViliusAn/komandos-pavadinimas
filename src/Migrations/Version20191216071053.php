<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191216071053 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE darbuotojas (id INT AUTO_INCREMENT NOT NULL, sandelis_id_id INT NOT NULL, pareigos INT NOT NULL, telefono_nr VARCHAR(255) NOT NULL, banko_saskaitos_nr VARCHAR(255) NOT NULL, INDEX IDX_217BA0426EBDA9E4 (sandelis_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE turto_tipas (id INT AUTO_INCREMENT NOT NULL, tipas VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pavedimo_operacija (id INT AUTO_INCREMENT NOT NULL, pavedimo_tipas_id INT NOT NULL, turtas_id INT NOT NULL, data DATETIME NOT NULL, UNIQUE INDEX UNIQ_5929D0B5292F6C6 (pavedimo_tipas_id), INDEX IDX_5929D0B56ADAFB (turtas_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pavedimo_tipas (id INT AUTO_INCREMENT NOT NULL, tipas VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE turtas (id INT AUTO_INCREMENT NOT NULL, tipas_id INT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, verte DOUBLE PRECISION NOT NULL, isigyjimo_data DATETIME NOT NULL, kiekis INT NOT NULL, UNIQUE INDEX UNIQ_376753301CEC0959 (tipas_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE skola (id INT AUTO_INCREMENT NOT NULL, tipas VARCHAR(255) NOT NULL, isdavimo_data DATETIME NOT NULL, grazinimo_data DATETIME NOT NULL, palukanos DOUBLE PRECISION NOT NULL, isdavejas VARCHAR(255) NOT NULL, dydis INT NOT NULL, grazinta_suma INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE darbuotojas ADD CONSTRAINT FK_217BA0426EBDA9E4 FOREIGN KEY (sandelis_id_id) REFERENCES sandelis (id)');
        $this->addSql('ALTER TABLE pavedimo_operacija ADD CONSTRAINT FK_5929D0B5292F6C6 FOREIGN KEY (pavedimo_tipas_id) REFERENCES pavedimo_tipas (id)');
        $this->addSql('ALTER TABLE pavedimo_operacija ADD CONSTRAINT FK_5929D0B56ADAFB FOREIGN KEY (turtas_id) REFERENCES turtas (id)');
        $this->addSql('ALTER TABLE turtas ADD CONSTRAINT FK_376753301CEC0959 FOREIGN KEY (tipas_id) REFERENCES turto_tipas (id)');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) DEFAULT NULL, CHANGE uzsake uzsake_id INT NOT NULL');
        $this->addSql('ALTER TABLE inventorius ADD CONSTRAINT FK_D9AE946C49F546A FOREIGN KEY (uzsake_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_D9AE946C49F546A ON inventorius (uzsake_id)');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE turtas DROP FOREIGN KEY FK_376753301CEC0959');
        $this->addSql('ALTER TABLE pavedimo_operacija DROP FOREIGN KEY FK_5929D0B5292F6C6');
        $this->addSql('ALTER TABLE pavedimo_operacija DROP FOREIGN KEY FK_5929D0B56ADAFB');
        $this->addSql('DROP TABLE darbuotojas');
        $this->addSql('DROP TABLE turto_tipas');
        $this->addSql('DROP TABLE pavedimo_operacija');
        $this->addSql('DROP TABLE pavedimo_tipas');
        $this->addSql('DROP TABLE turtas');
        $this->addSql('DROP TABLE skola');
        $this->addSql('ALTER TABLE inventorius DROP FOREIGN KEY FK_D9AE946C49F546A');
        $this->addSql('DROP INDEX IDX_D9AE946C49F546A ON inventorius');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE uzsake_id uzsake INT NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
