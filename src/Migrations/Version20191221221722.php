<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191221221722 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE atostogos ADD darbuotojas_id INT NOT NULL');
        $this->addSql('ALTER TABLE atostogos ADD CONSTRAINT FK_7CA576517C8F88E2 FOREIGN KEY (darbuotojas_id) REFERENCES darbuotojas (id)');
        $this->addSql('CREATE INDEX IDX_7CA576517C8F88E2 ON atostogos (darbuotojas_id)');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE atostogos DROP FOREIGN KEY FK_7CA576517C8F88E2');
        $this->addSql('DROP INDEX IDX_7CA576517C8F88E2 ON atostogos');
        $this->addSql('ALTER TABLE atostogos DROP darbuotojas_id');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
