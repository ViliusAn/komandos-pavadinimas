<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191222092354 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE darbo_kiekis (id INT AUTO_INCREMENT NOT NULL, tipas_id INT NOT NULL, zymejimasis_id INT NOT NULL, valandu_kiekis INT NOT NULL, INDEX IDX_5EFC36131CEC0959 (tipas_id), INDEX IDX_5EFC3613EAC81997 (zymejimasis_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE darbuotojas_dokumentas (darbuotojas_id INT NOT NULL, dokumentas_id INT NOT NULL, INDEX IDX_50C37A4A7C8F88E2 (darbuotojas_id), INDEX IDX_50C37A4AC2A8A77D (dokumentas_id), PRIMARY KEY(darbuotojas_id, dokumentas_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE darbuotojo_pareigos (id INT AUTO_INCREMENT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE atostogu_tipas (id INT AUTO_INCREMENT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE darbo_tipas (id INT AUTO_INCREMENT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dokumento_tipas (id INT AUTO_INCREMENT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE virsvalandziai (id INT AUTO_INCREMENT NOT NULL, darbuotojas_id INT NOT NULL, valandu_kiekis INT NOT NULL, registravimo_data DATETIME NOT NULL, INDEX IDX_AA4B646C7C8F88E2 (darbuotojas_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE atostogos (id INT AUTO_INCREMENT NOT NULL, tipas_id INT NOT NULL, darbuotojas_id INT NOT NULL, pradzios_data DATE NOT NULL, pabaigos_data DATE NOT NULL, INDEX IDX_7CA576511CEC0959 (tipas_id), INDEX IDX_7CA576517C8F88E2 (darbuotojas_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dokumentas (id INT AUTO_INCREMENT NOT NULL, tipas_id INT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, sudarymo_data DATETIME NOT NULL, aprasymas VARCHAR(5000) NOT NULL, url_adresas VARCHAR(500) NOT NULL, INDEX IDX_53DE1F0D1CEC0959 (tipas_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE darbo_kiekis ADD CONSTRAINT FK_5EFC36131CEC0959 FOREIGN KEY (tipas_id) REFERENCES darbo_tipas (id)');
        $this->addSql('ALTER TABLE darbo_kiekis ADD CONSTRAINT FK_5EFC3613EAC81997 FOREIGN KEY (zymejimasis_id) REFERENCES zymejimasis (id)');
        $this->addSql('ALTER TABLE darbuotojas_dokumentas ADD CONSTRAINT FK_50C37A4A7C8F88E2 FOREIGN KEY (darbuotojas_id) REFERENCES darbuotojas (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE darbuotojas_dokumentas ADD CONSTRAINT FK_50C37A4AC2A8A77D FOREIGN KEY (dokumentas_id) REFERENCES dokumentas (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE virsvalandziai ADD CONSTRAINT FK_AA4B646C7C8F88E2 FOREIGN KEY (darbuotojas_id) REFERENCES darbuotojas (id)');
        $this->addSql('ALTER TABLE atostogos ADD CONSTRAINT FK_7CA576511CEC0959 FOREIGN KEY (tipas_id) REFERENCES atostogu_tipas (id)');
        $this->addSql('ALTER TABLE atostogos ADD CONSTRAINT FK_7CA576517C8F88E2 FOREIGN KEY (darbuotojas_id) REFERENCES darbuotojas (id)');
        $this->addSql('ALTER TABLE dokumentas ADD CONSTRAINT FK_53DE1F0D1CEC0959 FOREIGN KEY (tipas_id) REFERENCES dokumento_tipas (id)');
        $this->addSql('DROP TABLE darbuotojas_zymejimasis');
        $this->addSql('ALTER TABLE darbuotojas ADD pareigos_id INT NOT NULL, DROP vardas, DROP pavarde, CHANGE pareigos naudotojas_id INT NOT NULL');
        $this->addSql('ALTER TABLE darbuotojas ADD CONSTRAINT FK_217BA04224666632 FOREIGN KEY (naudotojas_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE darbuotojas ADD CONSTRAINT FK_217BA0421DE90CC5 FOREIGN KEY (pareigos_id) REFERENCES darbuotojo_pareigos (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_217BA04224666632 ON darbuotojas (naudotojas_id)');
        $this->addSql('CREATE INDEX IDX_217BA0421DE90CC5 ON darbuotojas (pareigos_id)');
        $this->addSql('ALTER TABLE zymejimasis ADD darbuotojas_id INT NOT NULL, ADD isdirbtas_laikas INT NOT NULL, DROP atsizymejimo_laikas, DROP darbo_kiekis, DROP darbo_tipas, DROP virsvalandziai');
        $this->addSql('ALTER TABLE zymejimasis ADD CONSTRAINT FK_81F095277C8F88E2 FOREIGN KEY (darbuotojas_id) REFERENCES darbuotojas (id)');
        $this->addSql('CREATE INDEX IDX_81F095277C8F88E2 ON zymejimasis (darbuotojas_id)');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE darbuotojas DROP FOREIGN KEY FK_217BA0421DE90CC5');
        $this->addSql('ALTER TABLE atostogos DROP FOREIGN KEY FK_7CA576511CEC0959');
        $this->addSql('ALTER TABLE darbo_kiekis DROP FOREIGN KEY FK_5EFC36131CEC0959');
        $this->addSql('ALTER TABLE dokumentas DROP FOREIGN KEY FK_53DE1F0D1CEC0959');
        $this->addSql('ALTER TABLE darbuotojas_dokumentas DROP FOREIGN KEY FK_50C37A4AC2A8A77D');
        $this->addSql('CREATE TABLE darbuotojas_zymejimasis (darbuotojas_id INT NOT NULL, zymejimasis_id INT NOT NULL, INDEX IDX_694B5C71EAC81997 (zymejimasis_id), INDEX IDX_694B5C717C8F88E2 (darbuotojas_id), PRIMARY KEY(darbuotojas_id, zymejimasis_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE darbuotojas_zymejimasis ADD CONSTRAINT FK_694B5C717C8F88E2 FOREIGN KEY (darbuotojas_id) REFERENCES darbuotojas (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE darbuotojas_zymejimasis ADD CONSTRAINT FK_694B5C71EAC81997 FOREIGN KEY (zymejimasis_id) REFERENCES zymejimasis (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE darbo_kiekis');
        $this->addSql('DROP TABLE darbuotojas_dokumentas');
        $this->addSql('DROP TABLE darbuotojo_pareigos');
        $this->addSql('DROP TABLE atostogu_tipas');
        $this->addSql('DROP TABLE darbo_tipas');
        $this->addSql('DROP TABLE dokumento_tipas');
        $this->addSql('DROP TABLE virsvalandziai');
        $this->addSql('DROP TABLE atostogos');
        $this->addSql('DROP TABLE dokumentas');
        $this->addSql('ALTER TABLE darbuotojas DROP FOREIGN KEY FK_217BA04224666632');
        $this->addSql('DROP INDEX UNIQ_217BA04224666632 ON darbuotojas');
        $this->addSql('DROP INDEX IDX_217BA0421DE90CC5 ON darbuotojas');
        $this->addSql('ALTER TABLE darbuotojas ADD pareigos INT NOT NULL, ADD vardas VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD pavarde VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP naudotojas_id, DROP pareigos_id');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE zymejimasis DROP FOREIGN KEY FK_81F095277C8F88E2');
        $this->addSql('DROP INDEX IDX_81F095277C8F88E2 ON zymejimasis');
        $this->addSql('ALTER TABLE zymejimasis ADD atsizymejimo_laikas TIME NOT NULL, ADD darbo_kiekis INT NOT NULL, ADD darbo_tipas INT NOT NULL, ADD virsvalandziai INT DEFAULT NULL, DROP darbuotojas_id, DROP isdirbtas_laikas');
    }
}
