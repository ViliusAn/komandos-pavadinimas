<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191215004657 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE uzsakymas (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, busena_id INT DEFAULT NULL, siuntinys_id INT DEFAULT NULL, sukurimo_data DATETIME NOT NULL, pristatymo_data DATETIME DEFAULT NULL, patvirtinimo_data DATETIME DEFAULT NULL, adresas VARCHAR(255) NOT NULL, INDEX IDX_72B5827CA76ED395 (user_id), INDEX IDX_72B5827CC90D334F (busena_id), UNIQUE INDEX UNIQ_72B5827C97DB40A4 (siuntinys_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE siuntinys (id INT AUTO_INCREMENT NOT NULL, svoris DOUBLE PRECISION NOT NULL, aukstis DOUBLE PRECISION NOT NULL, plotis DOUBLE PRECISION NOT NULL, ilgis DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE uzsakymo_busena (id INT AUTO_INCREMENT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE uzsakymas ADD CONSTRAINT FK_72B5827CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE uzsakymas ADD CONSTRAINT FK_72B5827CC90D334F FOREIGN KEY (busena_id) REFERENCES uzsakymo_busena (id)');
        $this->addSql('ALTER TABLE uzsakymas ADD CONSTRAINT FK_72B5827C97DB40A4 FOREIGN KEY (siuntinys_id) REFERENCES siuntinys (id)');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE uzsakymas DROP FOREIGN KEY FK_72B5827C97DB40A4');
        $this->addSql('ALTER TABLE uzsakymas DROP FOREIGN KEY FK_72B5827CC90D334F');
        $this->addSql('DROP TABLE uzsakymas');
        $this->addSql('DROP TABLE siuntinys');
        $this->addSql('DROP TABLE uzsakymo_busena');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
