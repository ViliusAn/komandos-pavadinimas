<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191222171236 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE darbuotojas_dokumentas');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dokumentas ADD darbuotojas_id INT NOT NULL');
        $this->addSql('ALTER TABLE dokumentas ADD CONSTRAINT FK_53DE1F0D7C8F88E2 FOREIGN KEY (darbuotojas_id) REFERENCES darbuotojas (id)');
        $this->addSql('CREATE INDEX IDX_53DE1F0D7C8F88E2 ON dokumentas (darbuotojas_id)');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE darbuotojas_dokumentas (darbuotojas_id INT NOT NULL, dokumentas_id INT NOT NULL, INDEX IDX_50C37A4A7C8F88E2 (darbuotojas_id), INDEX IDX_50C37A4AC2A8A77D (dokumentas_id), PRIMARY KEY(darbuotojas_id, dokumentas_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE darbuotojas_dokumentas ADD CONSTRAINT FK_50C37A4A7C8F88E2 FOREIGN KEY (darbuotojas_id) REFERENCES darbuotojas (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE darbuotojas_dokumentas ADD CONSTRAINT FK_50C37A4AC2A8A77D FOREIGN KEY (dokumentas_id) REFERENCES dokumentas (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dokumentas DROP FOREIGN KEY FK_53DE1F0D7C8F88E2');
        $this->addSql('DROP INDEX IDX_53DE1F0D7C8F88E2 ON dokumentas');
        $this->addSql('ALTER TABLE dokumentas DROP darbuotojas_id');
        $this->addSql('ALTER TABLE inventorius CHANGE komentaras komentaras VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE user_type_id user_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_type CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
